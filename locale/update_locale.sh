#!/bin/bash

locales=(cs)

# Update the template
xgettext -d moovie -s -C -k_ -o moovie.pot ../src/*.cpp

# Update all the .po files
for loc in $locales; do
  msgmerge -vs -U $loc.po moovie.pot
done

echo "Don't forget to run 'make' after translating!"
exit 0
