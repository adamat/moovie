Moovie
======

Moovie aims to be an open source clone of Boovie. You probably have no idea
what that is. Boovie is an old Czech DOS logic game, you can get idea what it
is about from [this video](http://youtu.be/val2Pn04yAM).

Where it is now
---------------
It is now in heavy alpha state, few levels are done for testing purposes. Most
of the gameplay internals is done with a small numbers of planned elements
left. Currently, I am slowly working on embellishments like sounds and menus.
The program is still kind of _raw_, but surprisingly enjoyable. Monsters are
not implemented yet and the graphics are (hopefully) temporary.

Contribute
----------
Interested by the original game and want to join its clone's developement? Your
help is welcome! The game is mostly lacking in graphics, but in the future,
sounds and music will be needed too. If you were willing to spare some time to
get neverending fame as an artist of Moovie, don't hesitate to message me!

Dependencies
------------
Only libsfml-dev in version 2.1 (or higher) and GNU gettext as far as I rember;
everything else is contained within the repository and is built automatically.

But quite frankly, I have no idea of how portable it is. I use some unistd.h
calls in main.cpp to change working directory and GNU gettext.

Building
--------
Using CMake, see file `BUILDING`.

Used libraries
--------------
 * [SFML 2.2](http://sfml-dev.org) -- awesome multimedia library
 * [Thor](http://www.bromeon.ch/libraries/thor/index.html) -- extension to SFML
 * [GQE](https://code.google.com/p/gqe/) -- GatorQue Engine, a SFML game engine
