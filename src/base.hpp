/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BASE_H_6V6DJ1WR
#define BASE_H_6V6DJ1WR

#include <Thor/Animation.hpp>
#include <GQE/Core/assets/SoundAsset.hpp>
#include "collidable.hpp"

// Base as in baseball, not something elementary :)

namespace moovie {

class Base
: public Collidable
{
  private:
    static thor::FrameAnimation frame_anim;
    thor::Animator <sf::Sprite, int> animator;
    GQE::SoundAsset win_sound_buffer;
    sf::Sound win_sound;

  public:
    Base (StateLevel & parent, unsigned char x, unsigned char y);
    virtual ~Base ();

    static void setup();

    void on_frame(float frame_time);

    void collide_target(Collision & col, unsigned char side);
    bool stops_blood();
};

}

#endif /* end of include guard: BASE_H_6V6DJ1WR */
