/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "button.hpp"
#include "constants.hpp"
#include "state_level.hpp"

namespace moovie {

thor::FrameAnimation Button::anim_push_right[2];
thor::FrameAnimation Button::anim_push_left[2];
thor::FrameAnimation Button::anim_push_horisontal[2];
thor::FrameAnimation Button::anim_release_right[2];
thor::FrameAnimation Button::anim_release_left[2];
thor::FrameAnimation Button::anim_release_horisontal[2];

void Button::setup()
{
  anim_push_horisontal[0].addFrame(1.0f, {48, 288, 48, 24});
  anim_push_horisontal[0].addFrame(1.0f, {96, 288, 48, 24});

  anim_release_horisontal[0].addFrame(1.0f, {48, 288, 48, 24});
  anim_release_horisontal[0].addFrame(1.0f, { 0, 288, 48, 24});

  anim_push_right[0].addFrame(1.0f, {24, 336, 24, 48});
  anim_push_right[0].addFrame(1.0f, {48, 336, 24, 48});

  anim_release_right[0].addFrame(1.0f, {24, 336, 24, 48});
  anim_release_right[0].addFrame(1.0f, { 0, 336, 24, 48});

  anim_push_left[0].addFrame(1.0f, {96, 336, 24, 48});
  anim_push_left[0].addFrame(1.0f, {120, 336, 24, 48});

  anim_release_left[0].addFrame(1.0f, {96, 336, 24, 48});
  anim_release_left[0].addFrame(1.0f, {72, 336, 24, 48});

  anim_push_horisontal[1].addFrame(1.0f, {48, 312, 48, 24});
  anim_push_horisontal[1].addFrame(1.0f, {96, 312, 48, 24});

  anim_release_horisontal[1].addFrame(1.0f, {48, 312, 48, 24});
  anim_release_horisontal[1].addFrame(1.0f, { 0, 312, 48, 24});

  anim_push_right[1].addFrame(1.0f, {168, 336, 24, 48});
  anim_push_right[1].addFrame(1.0f, {192, 336, 24, 48});

  anim_release_right[1].addFrame(1.0f, {168, 336, 24, 48});
  anim_release_right[1].addFrame(1.0f, { 144, 336, 24, 48});

  anim_push_left[1].addFrame(1.0f, {240, 336, 24, 48});
  anim_push_left[1].addFrame(1.0f, {264, 336, 24, 48});

  anim_release_left[1].addFrame(1.0f, {240, 336, 24, 48});
  anim_release_left[1].addFrame(1.0f, {216, 336, 24, 48});
}

Button::Button(StateLevel & parent, unsigned char x, unsigned char y,
    unsigned char dir, unsigned char asset, unsigned char group) :
  Collidable(parent),
  Emitter(parent, group),
  dir_(dir)
{
  add_node(x, y);
  setPosition(x * NODE_SIZE, y * NODE_SIZE);

  unsigned char texture = asset;

  switch(dir){
    case UP:
      add_node(x + 1, y);
      animator.addAnimation(1, thor::refAnimation(anim_push_horisontal[asset]),
          sf::milliseconds(100));
      animator.addAnimation(2,
          thor::refAnimation(anim_release_horisontal[asset]),
          sf::milliseconds(150));
      break;
    case LEFT:
      add_node(x, y + 1);
      animator.addAnimation(1, thor::refAnimation(anim_push_left[asset]),
          sf::milliseconds(120));
      animator.addAnimation(2, thor::refAnimation(anim_release_left[asset]),
          sf::milliseconds(150));
      break;
    case RIGHT:
      add_node(x, y + 1);
      animator.addAnimation(1, thor::refAnimation(anim_push_right[asset]),
          sf::milliseconds(120));
      animator.addAnimation(2, thor::refAnimation(anim_release_right[asset]),
          sf::milliseconds(150));
      break;
  }

  auto sprite = mApp.make_sprite();
  push_back(sprite);

  animator.playAnimation(2, false);
  animator.update(sf::milliseconds(99));
  animator.animate(*sprite);
  animator.stopAnimation();

  project();

  push_sound_buffer.SetID("assets/sfx/button_push.flac");
  release_sound_buffer.SetID("assets/sfx/button_release.flac");
  push_sound.setBuffer(push_sound_buffer.GetAsset());
  release_sound.setBuffer(release_sound_buffer.GetAsset());

  fx = x + (dir_ == UP ? 0  : (dir_ == LEFT) ? -1 : 1);
  fy = y + (dir_ == UP ? -1 : 0);
  sx = fx+ (dir_ == UP ? 1  : 0);
  sy = fy+ (dir_ == UP ? 0  : 1);
}

Button::~Button()
{
}

void Button::push(bool force)
{
  if(!force and !second_press){
    second_press = true;
  }
  //Check, whether it's pressed by single object only
  else if(mLevel.get_node(fx, fy) == mLevel.get_node(sx, sy) or force){
    if(not was_pushed){
      animator.playAnimation(1);
      push_sound.play();
    }
    pushed = true;
    was_pushed = true;
    emit();
  }
}

void Button::collide_moovie(Collision & col, unsigned char side)
{
  side &= 3;
  if(dir_ == side)
    push();
  col.deny();
}

void Button::collide_brick(Collision & col, unsigned char side)
{
  side &= 3;
  if(dir_ == side and dir_ == UP)
    push();
  col.deny();
}

bool Button::stops_blood()
{
  return true;
}

bool Button::collide_ball()
{
  if(dir_ != UP)
    push(true);
  return 0;
}

void Button::on_frame(float frame_time)
{
  animator.update(sf::seconds(frame_time));
  animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(0)));
}

void Button::on_tick()
{
  second_press = false;

  if (not pushed and was_pushed and not animator.isPlayingAnimation()){
    animator.playAnimation(2, false);
    release_sound.play();
    was_pushed = false;
  }

  pushed = false;
}

} /* moovie */

