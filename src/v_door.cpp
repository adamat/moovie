/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "v_door.hpp"
#include "state_level.hpp"
#include "constants.hpp"

namespace moovie {

thor::FrameAnimation VerticalDoor::open_anim[2];
thor::FrameAnimation VerticalDoor::close_anim[2];

void VerticalDoor::setup()
{
  for(int i = 0; i < 9; i++){
    close_anim[0].addFrame(1.0f, {i * 24 + 480, 336, 24, 48});
    close_anim[1].addFrame(1.0f, {i * 24 + 696, 336, 24, 48});
  }

  for(int i = 8; i >= 0; i--){
    open_anim[0].addFrame(1.0f, {i * 24 + 480, 336, 24, 48});
    open_anim[1].addFrame(1.0f, {i * 24 + 696, 336, 24, 48});
  }
}

VerticalDoor::VerticalDoor(StateLevel & parent, unsigned char x,
    unsigned char y, sf::Time time_to_close, bool starts_closed,
    unsigned char asset) :
  Collidable(parent),
  closed(starts_closed)
{
  auto sprite = mApp.make_sprite();
  push_back(sprite);

  animator.addAnimation(0, thor::refAnimation(open_anim[asset]),
      sf::milliseconds(300));
  animator.addAnimation(1, thor::refAnimation(close_anim[asset]),
      sf::milliseconds(300));

  init_xy(x, y, 0, 1);

  if(closed){
    timeout = time_to_close.asSeconds() * UPDATE_RATE;
    animator.playAnimation(0);
    animator.update(sf::milliseconds(1));
    animator.animate(*sprite);
  }
  else{
    wipe(); // doesn't delete the node's information
    timeout = 0;
    animator.playAnimation(1);
    animator.update(sf::milliseconds(1));
    animator.animate(*sprite);
  }
}


void VerticalDoor::activate()
{
  if(not not_yet){
    if(!closed and mLevel.get_node(tlx, tly) == mLevel.air
        and mLevel.get_node(brx, bry) == mLevel.air){
      project(); // YOU SHALL NOT PASS!
      /*
       *       /|
       * o   -----   |
       * | ___\"/___ |
       * | |,-   -.| +
       * |    / \    '
       * |   /___\
       *      ' '
       */
      not_yet = true;
      closed = true;
      animator.playAnimation(1);
    }
    else if(closed){
      not_yet = true;
      closed = false;
      animator.playAnimation(0);
    }
  }
}


void VerticalDoor::on_tick()
{
  if(delay and delay++ > timeout){
    activate(); // = close
    if(closed) //Check, whether the closing has started
      delay = 0;
    else
      delay = timeout;
  }
}

void VerticalDoor::on_frame(float frame_time)
{
  if(not_yet){
    animator.update(sf::seconds(frame_time));
    animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(0)));
    if(not animator.isPlayingAnimation()){
      not_yet = false;
      if(not closed){
        wipe();
        if(timeout)
          delay = 1;
      }
    }
  }
}

VerticalDoor::~VerticalDoor()
{
}

} /* moovie */

