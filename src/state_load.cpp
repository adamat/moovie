/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unordered_map>
#include <GQE/Core/assets/ImageAsset.hpp>
#include "state_load.hpp"
#include "app.hpp"
#include "constants.hpp"
#include "glass.hpp"
#include "base.hpp"
#include "creature.hpp"
#include "moovie.hpp"
#include "lift.hpp"
#include "button.hpp"
#include "teleport.hpp"
#include "blood.hpp"
#include "v_door.hpp"

namespace moovie {

StateLoad::StateLoad(GQE::IApp & parent) :
  GQE::IState("Loading", parent),
  mImageHandler(mApp.mAssetManager.GetHandler<sf::Texture>())
{
}

void StateLoad::load()
{
  mImageHandler.GetReference("atlas");
  mImageHandler.SetFilename("atlas", "assets/atlas.png");
  mImageHandler.DropReference("atlas", GQE::AssetDropAtExit);

  // Setup static class data (i.e. animations)
  Glass::setup();
  Base::setup();
  Creature::setup();
  Moovie::setup();
  Lift::setup();
  Button::setup();
  Teleport::setup();
  Blood::setup();
  VerticalDoor::setup();
}

void StateLoad::DoInit()
{
  IState::DoInit();
  load();
  mImageHandler.LoadAllAssets();
}

void StateLoad::ReInit()
{
}

void StateLoad::UpdateFixed()
{
  mApp.mStateManager.RemoveActiveState();
}

void StateLoad::UpdateVariable(float ft)
{
}

void StateLoad::Draw()
{
}

void StateLoad::HandleCleanup()
{
}

StateLoad::~StateLoad()
{
}

} /* moovie */
