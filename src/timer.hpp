/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TIMER_H_ZZL19AXZ
#define TIMER_H_ZZL19AXZ

#include <SFML/System/Time.hpp>

namespace moovie {

class Receiver;

class Timer
{
  private:
    const unsigned int timeout_;
    unsigned int remaining_; // how many ticks remain until timeout
    Receiver *timee;

  public:
    Timer (Receiver *target, sf::Time timeout);
    virtual ~Timer ();

    void reset();
    void tick();
};

}

#endif /* end of include guard: TIMER_H_ZZL19AXZ */
