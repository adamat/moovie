/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fader.hpp"
#include "constants.hpp"

namespace moovie {

void anim_fade_out(sf::RectangleShape & animated, float progress)
{
  animated.setFillColor({0, 0, 0, 255 * progress});
}

void anim_fade_in(sf::RectangleShape & animated, float progress)
{
  animated.setFillColor({0, 0, 0, 255 - 255 * progress * progress});
}

Fader::Fader(sf::Time fading_time) :
  shield({WIN_WIDTH, WIN_HEIGHT})
{
  /* Fade out */
  animator.addAnimation(0, thor::refAnimation(anim_fade_out), fading_time);

  /* Fade in */
  animator.addAnimation(1, thor::refAnimation(anim_fade_in), fading_time);

  shield.setFillColor({0, 0, 0, 0});
}

void Fader::fade_out(callback_t function)
{
  if(not fading){
    animator.playAnimation(0);
    callback = function;
    fading = true;
  }
}

void Fader::fade_in()
{
  if(not fading){
    animator.playAnimation(1);
    callback = nullptr;
    fading = true;
  }
}

void Fader::tick()
{
}


void Fader::frame(float frame_time)
{
  if(fading){
    animator.update(sf::seconds(frame_time));
    animator.animate(shield);

    if(not animator.isPlayingAnimation()){
      fading = false;
      if(callback)
        callback();
      /* This brutal hack was added, because Thor doesn't run the "final
       * frame", i.e., when there is a lag and the animation stops after one
       * (or so) frame, the shield would stay dark. No callback means, that the
       * fader was fading IN. Were it to fade OUT with no callback, an
       * additional information would be needed, but that's not the case yet */
      else
        shield.setFillColor({0, 0, 0, 0});
    }
  }
}

void Fader::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  /*
  if(fading){
    target.draw(shield, states);
  }
  */
  target.draw(shield, states);
}

Fader::~Fader()
{
}

} /* moovie */

