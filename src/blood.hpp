/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLOOD_H_8JGRX11S
#define BLOOD_H_8JGRX11S

#include <Thor/Animation.hpp>
#include "node.hpp"

namespace moovie {

class Blood :
  public Node
{
  private:
    float vx, vy; // vector of movement
    float x, y; // position
    float tx, ty; // temporary

    const float friction = 0.6;
    const float y_bounce = 0.1f;
    const float x_bounce = 0.3f;
    const float ground_friction = 250.0f;
    const float gravity = 390.0f;

    static thor::FrameAnimation frame_anim;
    thor::Animator <sf::Sprite, bool> animator;

    bool y_stopped = 0;
    bool x_stopped = 0;

  public:
    Blood(StateLevel & parent, unsigned x_, unsigned y_, float vx_, float vy_);
    virtual ~Blood ();

    static void setup();

    void on_frame(float frame_time);
};

} /* moovie */

#endif /* end of include guard: BLOOD_H_8JGRX11S */
