/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "collision.hpp"
#include "creature.hpp"

namespace moovie {

void Collision::approve()
{
  ;
  // exists for potential collision system change
}


void Collision::absolute_approve()
{
  approved = true;
  // this is called by bricks that notice two consequent collisions and can move
}


void Collision::deny(bool lethal)
{
  if(lethal){
    must_die = true;
  }
  else
    approved = false;
}


void Collision::kill(Creature *cr)
{
  to_be_killed.insert(cr);
}


void Collision::set_max_velocity(float v)
{
  if(v < velocity_ or !velocity_){
    velocity_ = v;
  }
}


bool Collision::ok()
{
  if(must_die or !approved){
    return false; // no, sorry, you can't move (maybe not live either)
  }
  else{
    for(auto cr : to_be_killed ){
      cr->die();
    }

    return true;
  }
}


bool Collision::lethal()
{
  return must_die;
}


float Collision::velocity()
{
  return velocity_;
}

}

