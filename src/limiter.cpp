/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "limiter.hpp"
#include "constants.hpp"

namespace moovie {

Limiter::Limiter(StateLevel & parent) :
  mLevel(parent)
{
  ;
}

Limiter::~Limiter()
{
  ;
}


void Limiter::frame(float ft)
{
  std::vector< unsigned short > to_delete;
  for(auto &i : limits){
    i.second.cd -= (i.second.v * ft);
    if(i.second.cd <= 0){
      to_delete.push_back(i.first);
    }
  }
  for(auto k : to_delete)
    delete_key(k);
}

void Limiter::delete_key(unsigned short k)
{
  limits.erase(k);
  unsigned char x, y;
  k >>= 5;
  y = k & 0x1f;
  k >>= 5;
  x = k & 0x1f;
  if(mLevel.get_node(x, y) == mLevel.limited)
    mLevel.set_node(x, y, mLevel.air);
}


unsigned char Limiter::get_limit_speed(unsigned char x, unsigned char y,
    unsigned char direction)
{
  unsigned k = make_key(x, y, direction);

  if(limits.count(k)){
    Limit l = limits[k];
    return l.v;
  }
  else
    return 0;
}


unsigned char Limiter::get_remaining_pixels(unsigned char x, unsigned char y,
    unsigned char direction)
{
  unsigned k = make_key(x, y, direction);

  if(limits.count(k)){
    Limit l = limits[k];
    return (NODE_SIZE - l.cd);
  }
  else
    return 0;
}


void Limiter::set_limit(unsigned char x, unsigned char y, unsigned char
    direction, unsigned char limit)
{
  Limit l;
  l.v = limit;
  l.cd = NODE_SIZE;

  limits[make_key(x, y, direction)] = l;
  mLevel.set_node(x, y, mLevel.limited);
}

unsigned short Limiter::make_key(unsigned char x, unsigned char y,
    unsigned char d)
{
  unsigned short tmp = x;
  tmp <<= 5;
  tmp |= y;
  tmp <<= 5;
  tmp |= d;

  return tmp;
}

}

