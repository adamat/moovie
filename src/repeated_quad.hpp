/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REPEATED_QUAD_H_8BCCCJA1
#define REPEATED_QUAD_H_8BCCCJA1

#include <SFML/Graphics.hpp>
#include "node.hpp"

namespace moovie {

class RepeatedQuad :
  public sf::Drawable,
  public sf::Transformable,
  public INode
{
  private:
    sf::VertexArray va;
    sf::Texture *tex;
    sf::FloatRect r;
    bool left;

  protected:

  public:
    RepeatedQuad(sf::Texture * texture, sf::FloatRect subrect, bool add_on_left);
      // add_on_left = the pusher/door is extending rightwards
    virtual ~RepeatedQuad ();

    /* returns, wheter the current quad has finished growing */
    bool change_size(float size_delta);

    inline void tick() override{}
    inline void frame(float) override{}
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
};

} /* moovie */

#endif /* end of include guard: REPEATED_QUAD_H_8BCCCJA1 */
