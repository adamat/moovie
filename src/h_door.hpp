/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_DOOR_H_MLUDUXD5
#define H_DOOR_H_MLUDUXD5

#include "machine.hpp"
#include "repeated_quad.hpp"

namespace moovie {

class HorizontalDoor :
  public Machine
{
  private:
    bool to_left;
    bool will_move = false;
    bool moving = false;
    bool try_closing = false;
    bool back = false;
    bool will_back = false;
    bool force = false;
    unsigned char length;
    unsigned char length_counter = 0;
    unsigned char timeout;
    unsigned char delay = 0;
    const float velocity = 80.0f;
    bool pending_collision = false; // To check self-collisions
    RepeatedQuad * body;

  public:
    HorizontalDoor (StateLevel & parent, unsigned char x, unsigned char y,
        unsigned char tox, sf::Time time_to_close, bool closed,
        unsigned char asset);
    virtual ~HorizontalDoor ();

    void on_tick();
    void on_frame(float frame_time);
    void activate();

    void collide_machine(Collision & col, unsigned char side);
};

} /* moovie */

#endif /* end of include guard: H_DOOR_H_MLUDUXD5 */
