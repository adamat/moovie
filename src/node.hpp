/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/******************
  Node in scene graph
 ******************/

#ifndef NODE_H_3NL0N2FJ
#define NODE_H_3NL0N2FJ

#include <vector>
#include <SFML/Graphics.hpp>
#include <Thor/Animation.hpp>
#include "app.hpp"

namespace moovie {

//Forward declarations
class StateLevel;

class INode
{
  private:
  protected:
  public:
    INode();
    virtual ~INode ();
    virtual void tick() = 0;
    virtual void frame(float frame_time) = 0;
    virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const = 0;
};

class Node :
  public INode,
  public sf::Transformable,
  protected std::vector<INode*>
{
  friend class StateLevel;
  friend class StateMenu;
  private:
  protected:
    // use these to do some actions
    virtual void on_tick();
    virtual void on_frame(float frame_time); // is called just before draw() is
    virtual void on_draw(sf::RenderTarget &target, sf::RenderStates states)
      const;

    // parent state (for collisions etc.)
    StateLevel & mLevel;
    moovie::App & mApp;
  public:
    Node();
    Node(StateLevel & parent);
    virtual ~Node();

    // these three propagate the events to all the children automatically
    // tick() is called periodically, about 20 times per second
    void tick() override;
    // these two are called irregularly, depending on the machine's performance
    void frame(float frame_time) override; // is called just before draw() is
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
};

template <class TYPE>
class DrawableNode :
  public INode,
  public TYPE
{
  private:
  protected:
  public:
    using TYPE::TYPE;
    virtual ~DrawableNode () {}
    inline void draw(sf::RenderTarget &target, sf::RenderStates states) const
      override {
        target.draw((TYPE) *this, states);
      }
    void tick() override {}
    void frame(float) override {}
};

template <class Animated, typename Id>
class AnimatorNode :
  public INode,
  public thor::Animator<Animated, Id>
{
  private:
  protected:
    Animated & animated;
  public:
    AnimatorNode(Animated & a) :
      animated(a) {}
    virtual ~AnimatorNode() {}
    void draw(sf::RenderTarget &target, sf::RenderStates states) const
      override {}
    void tick() override {}
    void frame(float dtime) override {
      thor::Animator<Animated, Id>::update(sf::seconds(dtime));
      thor::Animator<Animated, Id>::animate(animated);
    }
};

} //namespace moovie

#endif /* end of include guard: NODE_H_3NL0N2FJ */
