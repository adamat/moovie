/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIFT_H_Y3ZLFSK6
#define LIFT_H_Y3ZLFSK6

#include <functional>
#include <SFML/System/Time.hpp>
#include <Thor/Animation.hpp>
#include <GQE/Core/assets/SoundAsset.hpp>
#include "machine.hpp"

namespace moovie {

class LiftBase
: public Machine
{
  private:
    unsigned char from_, to_; // limit coordinates
    bool vert;

  protected:
    float speed;
    unsigned char dir;
    bool moving = false;
    bool will_start = false;
    void start();

    GQE::SoundAsset start_sound_buffer;
    sf::Sound start_sound;
    GQE::SoundAsset stop_sound_buffer;
    sf::Sound stop_sound;

  public:
    LiftBase (StateLevel & parent, unsigned char x, unsigned char y,
        unsigned char to, bool vertical, sf::Time timeout);
    virtual ~LiftBase ();

    void on_frame(float frame_time);
    void activate();
};

class Lift
: public LiftBase
{
  private:
    static std::function<void(sf::Sprite&, float)> rotate_anim;
    thor::Animator <sf::Sprite, int> animator;

  protected:
  public:
    Lift (StateLevel & parent, unsigned char x, unsigned char y,
        unsigned char to_y, sf::Time timeout, unsigned char asset);
    virtual ~Lift ();

    static void setup();

    void on_frame(float frame_time);
    void collide_brick(Collision & col, unsigned char side);
    void collide_moovie(Collision & col, unsigned char side);
    int get_layer() const;
};

class Platform
: public LiftBase
{
  private:
    bool attempt_step(unsigned char d); //overwrite from Collidable

  public:
    Platform (StateLevel & parent, unsigned char x, unsigned char y,
        unsigned char to_x, sf::Time timeout, unsigned char asset);
    virtual ~Platform ();
};

} /* moovie */

#endif /* end of include guard: LIFT_H_Y3ZLFSK6 */

