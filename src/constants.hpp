/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONSTANTS_H_5T8079NN
#define CONSTANTS_H_5T8079NN

/* Some widely used constants */
namespace moovie {

// Orientation, negating results in the opposite direction
// Down and right share the lowest bit

enum {
  UP = 0,
  DOWN = 3,
  RIGHT = 1,
  LEFT = 2
};

// Number of in-game nodes ("little squares") per axis
const unsigned char NODES_X = 32;
const unsigned char NODES_Y = 22;

const unsigned char NODE_SIZE = 24; //pixes in one axis per node

const unsigned int WIN_WIDTH = 768;
const unsigned int WIN_HEIGHT = 592;

const unsigned int UPDATE_RATE = 20; // ticks per second

}

#endif /* end of include guard: CONSTANTS_H_5T8079NN */
