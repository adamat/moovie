/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "base.hpp"
#include "state_level.hpp"
#include "constants.hpp"

namespace moovie {

thor::FrameAnimation Base::frame_anim;

void Base::setup()
{
  frame_anim.addFrame(1.0f, {768 + 0, 432, 48, 18});
  frame_anim.addFrame(1.0f, {768 + 48, 432, 48, 18});
  frame_anim.addFrame(1.0f, {768 + 96, 432, 48, 18});
  frame_anim.addFrame(1.0f, {768 + 144, 432, 48, 18});
}

Base::Base(StateLevel & parent, unsigned char x, unsigned char y) :
  Collidable(parent)
{
  auto sprite = mApp.make_sprite();
  push_back(sprite);
  setPosition(x * NODE_SIZE, y * NODE_SIZE);

  animator.addAnimation(0, thor::refAnimation(frame_anim),
      sf::milliseconds(400));
  animator.playAnimation(0, true);

  win_sound_buffer.SetID("assets/sfx/win.flac");
  win_sound.setBuffer(win_sound_buffer.GetAsset());

  add_node(x, y);
  add_node(x + 1, y);
  project();

  tly = bry = y;
  tlx = x;
  brx = x + 1;
}

Base::~Base()
{
  //Do NOT delete 'sprite'
  ;
}

void Base::on_frame(float frame_time)
{
  animator.update(sf::seconds(frame_time));
  animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(0)));
}

void Base::collide_target(Collision & col, unsigned char side)
{
  side &= 3; //clean
  if(side == UP and !mLevel.won()){
    if(mLevel.get_node(tlx, tly-1) == mLevel.get_node(brx, tly-1)){
      mLevel.win();
      win_sound.play();
    }
  }

  col.deny();
}

bool Base::stops_blood()
{
  return true;
}

}

