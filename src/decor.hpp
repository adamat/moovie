/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DECOR_H_W8JVARMG
#define DECOR_H_W8JVARMG

#include "drawable.hpp"

// Animation type byte:
// O O O O  O O O O
// | | `-`--`-`-`-`-- HALF the delay between repeats (0..63 -> 0..126)
// |  `-------------- Return (Alone or mixed with ONCE or delay)
//  `---------------- Once (Alone or mixed with RETURN)
//  If delay is given, the animation is also delayed before the start

enum{
  ONCE = 1 << 7,  // Play the animation once, then keep showing last frame
  RETURN = 1 << 6 // Play to the end, then backward to the begin, looped
};

// Possibilities:
// < nothing > - Looped animation w/ possible delay
// ONCE        - Play only once, then keep showing the last frame
// RETURN      - See the flag description
// ONCE|RETURN - Like return, but only once

class Decor : public Drawable
{
  private:
    unsigned char flags; //type
    unsigned char frames, frame = 1;
    bool slow, deadframe = 0;
    bool backwards = 0;
    bool autodraw_ = 1;
    int layer_;
    unsigned char delay, delayer = 0; //delay value and counter
    std::map<std::string, unsigned char> views; //name, flags

  public:
    // First five arguments are passed to masdl::Sprite::Set()
    Decor (const char* path, unsigned fw, unsigned fh, unsigned fxc, unsigned
        fyc, unsigned short x, unsigned short y, unsigned char anim_type, bool
        slow_ = false, int layer = 0);
    virtual ~Decor ();

    void make_view(const char* name, unsigned row, unsigned frames, unsigned
        char flags_);
    void set_view(const char* name);
    void draw();
    void autodraw(bool ad);
    int get_layer() const;
};

#endif /* end of include guard: DECOR_H_W8JVARMG */
