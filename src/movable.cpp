/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "movable.hpp"
#include "state_level.hpp"
#include "constants.hpp"

namespace moovie {

Movable::Movable(StateLevel & parent) :
  Collidable(parent)
{
  ;
}

Movable::~Movable()
{
  ;
}

void Movable::limit_speed(float vel)
{
  unsigned char x, y;
  unsigned char d = get_direction();
  switch(d){
    case UP:
    case DOWN:
      y = (d == DOWN)?(tly - 1):(bry + 1);
      for(x = tlx; x <= brx; x++){
        if(mLevel.get_node(x, y) == mLevel.air){
          mLevel.set_node(x, y, mLevel.limited);
          mLevel.limiter->set_limit(x, y, get_direction(), vel);
        }
      }
      break;
    case LEFT:
    case RIGHT:
      x = (d == RIGHT)?(tlx - 1):(brx + 1);
      for(y = tly; y <= bry; y++){
        if(mLevel.get_node(x, y) == mLevel.air){
          mLevel.set_node(x, y, mLevel.limited);
          mLevel.limiter->set_limit(x, y, get_direction(), vel);
        }
      }
      break;
    default:
      std::cerr<<"Invalid direction! ";
  }
}

bool Movable::attempt_move(float velocity)
{
  set_velocity(velocity);
  if(attempt_step(direction_)){ //incl. collisions
    countdown_ = NODE_SIZE - lpixels_;
    if(lvelocity_) limit_speed(lvelocity_); //bubble
    return true;
  }
  else return false;
}


void Movable::do_moving(float frame_time)
{
  if((!lpixels_) and lvelocity_){
    set_velocity(lvelocity_);
    lvelocity_ = 0;
  }

  if(lpixels_ > 0){
    /*
    if(countdown_ == 0 or countdown_ > NODE_SIZE){
      lvelocity_ = 0;
      countdown_ = 0;
      lpixels_ = 0;
    }
    else{
    */
      lpixels_ += lvelocity_ * frame_time;
      countdown_ -= lvelocity_ * frame_time;
    //}
  }

  float & count = lvelocity_ ? lpixels_ : countdown_;
  float v = get_velocity();
  float delta = (count >= v * frame_time) ? v * frame_time : count;
  if(direction_ == UP){
    move(0, 0 - delta);
  }
  if(direction_ == DOWN){
    move(0, delta);
  }
  if(direction_ == LEFT){
    move(0 - delta, 0);
  }
  if(direction_ == RIGHT){
    move(delta, 0);
  }

  count -= delta;

}


bool Movable::moving_done() const
{
  return ! bool(countdown_ + lpixels_); // true if countdown_ == 0
}


void Movable::set_direction(unsigned char d)
{
  direction_ = d & 3; // only the important bits
}

unsigned char Movable::get_direction() const
{
  return direction_;
}

/*
bool Movable::keep_moving()
{
  if(! acted){
    if(! is_done()){
      acted = true;
      move(); //there's some moving to do
      return true;
    }
    else return false;
  }
  else return true;
}
*/

void Movable::on_frame(float frame_time)
{
  if(not moving_done())
    do_moving(frame_time);
}

void Movable::teleport(unsigned char x, unsigned char y)
{
  wipe();
  clear_nodes();
  init_xy(x, y, 1, 1);
}

}

