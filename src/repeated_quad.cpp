/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "repeated_quad.hpp"

namespace moovie {

RepeatedQuad::RepeatedQuad(sf::Texture * texture, sf::FloatRect subrect,
    bool add_on_left) :
  va(sf::Quads),
  tex(texture),
  r(subrect),
  left(add_on_left)
{
  if(not left){
    va.append(sf::Vertex({0, 0}, {r.left, r.top}));
    va.append(sf::Vertex({0, r.height}, {r.left, r.top + r.height}));
    va.append(sf::Vertex({0, r.height}, {r.left, r.top + r.height}));
    va.append(sf::Vertex({0, 0}, {r.left, r.top}));
  }
  else{
    va.append(sf::Vertex({0, r.height}, {r.left + r.width, r.top + r.height}));
    va.append(sf::Vertex({0, 0}, {r.left + r.width, r.top}));
    va.append(sf::Vertex({0, 0}, {r.left + r.width, r.top}));
    va.append(sf::Vertex({0, r.height}, {r.left + r.width, r.top + r.height}));
  }
}

bool RepeatedQuad::change_size(float size_delta)
{
  float & d = size_delta;
  if(not left){
    // Create new quad
    if(d > 0.0f and
        va[va.getVertexCount() - 1].texCoords.x >= r.left + r.width){
      sf::Vertex & v1 = va[va.getVertexCount() - 1];
      va.append(sf::Vertex({v1.position.x, 0}, {r.left, r.top}));
      va.append(sf::Vertex({v1.position.x, r.height},
            {r.left, r.top + r.height}));
      va.append(sf::Vertex({v1.position.x, r.height},
            {r.left, r.top + r.height}));
      va.append(sf::Vertex({v1.position.x, 0}, {r.left, r.top}));
    }
    else if(d < 0.0f and va[va.getVertexCount() - 1].texCoords.x <= r.left){
      va.resize(va.getVertexCount() - 4);
      if(va.getVertexCount() == 0){
        va.append(sf::Vertex({0, 0}, {r.left, r.top}));
        va.append(sf::Vertex({0, r.height}, {r.left, r.top + r.height}));
        va.append(sf::Vertex({0, r.height}, {r.left, r.top + r.height}));
        va.append(sf::Vertex({0, 0}, {r.left, r.top}));
      }
    }

    // Fetch the last two vertices
    sf::Vertex & v1 = va[va.getVertexCount() - 1];
    sf::Vertex & v2 = va[va.getVertexCount() - 2];

    // New quad would be created / deleted
    if(d > 0.0f and v1.texCoords.x + d >= r.left + r.width){
      float remain = v1.texCoords.x + d - (r.left + r.width);
      v1.position.x += d - remain;
      v2.position.x += d - remain;
      v1.texCoords.x += d - remain;
      v2.texCoords.x += d - remain;
      return true;
    }
    else if(d < 0.0f and v1.texCoords.x + d <= r.left){
      float d2 = v1.texCoords.x - r.left;
      v1.position.x -= d2;
      v2.position.x -= d2;
      v1.texCoords.x -= d2;
      v2.texCoords.x -= d2;
      return true;
    }
    else{ // we will fit just fine
      v1.position.x += d;
      v2.position.x += d;
      v1.texCoords.x += d;
      v2.texCoords.x += d;
      return false;
    }
  }
  else{/* left == true */
    // Create new quad
    if(d > 0.0f and
        va[va.getVertexCount() - 3].texCoords.x <= r.left){
      sf::Vertex & v1 = va[va.getVertexCount() - 3];
      va.append(sf::Vertex({v1.position.x, 0}, {r.left + r.width, r.top}));
      va.append(sf::Vertex({v1.position.x, r.height},
            {r.left + r.width, r.top + r.height}));
      va.append(sf::Vertex({v1.position.x, r.height},
            {r.left + r.width, r.top + r.height}));
      va.append(sf::Vertex({v1.position.x, 0}, {r.left + r.width, r.top}));
    }
    else if(d < 0.0f and
        va[va.getVertexCount() - 3].texCoords.x >= r.left + r.width){
      va.resize(va.getVertexCount() - 4);
      if(va.getVertexCount() == 0){
        va.append(sf::Vertex({0, r.height}, {r.left+r.width, r.top+r.height}));
        va.append(sf::Vertex({0, 0}, {r.left + r.width, r.top}));
        va.append(sf::Vertex({0, 0}, {r.left + r.width, r.top}));
        va.append(sf::Vertex({0, r.height}, {r.left+r.width, r.top+r.height}));
      }
    }

    // Fetch the two vertices being changed
    sf::Vertex & v1 = va[va.getVertexCount() - 3];
    sf::Vertex & v2 = va[va.getVertexCount() - 4];

    // New quad would be created / deleted
    if(d > 0.0f and v1.texCoords.x - d <= r.left){
      float d2 = v1.texCoords.x - r.left;
      v1.position.x -= d2;
      v2.position.x -= d2;
      v1.texCoords.x -= d2;
      v2.texCoords.x -= d2;
      return true;
    }
    else if (d < 0.0f and v1.texCoords.x - d >= r.left + r.width){
      float remain = v1.texCoords.x + d - (r.left + r.width);
      v1.position.x += d - remain;
      v2.position.x += d - remain;
      v1.texCoords.x += d - remain;
      v2.texCoords.x += d - remain;
      return true;
    }
    else{ // we will fit just fine
      v1.position.x -= d;
      v2.position.x -= d;
      v1.texCoords.x -= d;
      v2.texCoords.x -= d;
      return false;
    }
  }
}

RepeatedQuad::~RepeatedQuad()
{
}

void RepeatedQuad::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  states.transform *= getTransform();
  states.texture = tex;
  target.draw(va, states);
}

} /* moovie */

