/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MACHINE_H_XFDC4YHX
#define MACHINE_H_XFDC4YHX

#include "movable.hpp"
#include "receiver.hpp"
#include "timer.hpp"

namespace moovie {

class Machine : public Movable, public Receiver
{
  protected:
    Timer timer;

  public:
    Machine (StateLevel & parnent, sf::Time timeout);
    virtual ~Machine ();

    void on_tick();
    void teleport(unsigned char, unsigned char); //ignore teleportation
};

} /*moovie*/

#endif /* end of include guard: MACHINE_H_XFDC4YHX */
