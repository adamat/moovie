/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUSHER_H_YQ35WP50
#define PUSHER_H_YQ35WP50

#include "machine.hpp"
#include "repeated_quad.hpp"

namespace moovie {

class Pusher :
  public Machine
{
  private:
    bool to_left;
    bool will_move = false;
    bool moving = false;
    bool back = false;
    bool will_back = false;
    unsigned char length;
    unsigned char length_counter = 0;
    unsigned char delay = 0;
    const float velocity = 80.0f;
    bool pending_collision = false; // To check self-collisions
    RepeatedQuad * body;

  public:
    Pusher (StateLevel & parent, unsigned char x, unsigned char y,
        unsigned char tox, unsigned char asset);
    virtual ~Pusher ();

    void on_tick();
    void on_frame(float frame_time);
    void activate();

    void collide_machine(Collision & col, unsigned char side);
};

} /* moovie */

#endif /* end of include guard: PUSHER_H_YQ35WP50 */
