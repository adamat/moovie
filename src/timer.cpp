/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timer.hpp"
#include "constants.hpp"
#include "receiver.hpp"

/* Yes, that's right, I keep using the old ugly 'ticks' (fixed update rate) for
 * time measurement. What a shame, isn't it? Or is it? Do I really need precise
 * timer? Isn't a twentieth of second precise enough?
 */

namespace moovie {

Timer::Timer(Receiver *target, sf::Time timeout):
  timeout_(timeout.asSeconds() * UPDATE_RATE),
  remaining_(timeout.asSeconds() * UPDATE_RATE),
  timee(target)
{
}

Timer::~Timer()
{
}

void Timer::reset()
{
  remaining_ = timeout_;
}

void Timer::tick()
{
  if(!remaining_) return;
  if(--remaining_ == 0){
    timee->activate();
  }
}

}

