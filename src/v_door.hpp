/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef V_DOOR_H_35DVYYWK
#define V_DOOR_H_35DVYYWK

#include <Thor/Animation.hpp>
#include "collidable.hpp"
#include "receiver.hpp"

namespace moovie {

class VerticalDoor :
  public Collidable,
  public Receiver
{
  private:
    unsigned char timeout;
    unsigned char delay = 0;
    bool closed;
    bool not_yet = false; // just closing/opening

    static thor::FrameAnimation open_anim[2];
    static thor::FrameAnimation close_anim[2];
    thor::Animator <sf::Sprite, bool> animator;

  public:
    VerticalDoor (StateLevel & parent, unsigned char x, unsigned char y,
        sf::Time time_to_close, bool starts_closed, unsigned char asset);
    virtual ~VerticalDoor ();

    static void setup();

    void activate();
    void on_tick();
    void on_frame(float frame_time);
};

} /* moovie */

#endif /* end of include guard: V_DOOR_H_35DVYYWK */

