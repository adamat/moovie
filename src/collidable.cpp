/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "collidable.hpp"
#include "state_level.hpp"

namespace moovie {

Collidable::Collidable(StateLevel & parent) :
  Node(parent)
{
  ;
}

Collidable::~Collidable()
{
  wipe();
}

void Collidable::init_xy(unsigned char x, unsigned char y, unsigned char w,
    unsigned char h)
{
  setPosition(NODE_SIZE * x, NODE_SIZE * y);

  //Add collidable nodes
  for(int yy = y; yy <= y + h; yy++)
    for(int xx = x; xx <= x + w; xx++)
      add_node(xx, yy);

  project(); // place those nodes on scene

  tlx = x;
  tly = y;
  brx = x + w;
  bry = y + h;

}

bool Collidable::attempt_step(unsigned char d)
{
  // Clean d
  d &= 3;
  lvelocity_ = lpixels_ = 0;
  // Collision testing
  Collision col;
  col.set_max_velocity(get_velocity());
  unsigned char x, y;
  Collidable *tmp;
  switch(d){ //setup deltas
    case UP:
    case DOWN:
      y = (d == UP)?(tly - 1):(bry + 1);
      for(x = tlx; x <= brx; x++){
        tmp = mLevel.get_node(x, y);
        if(tmp == mLevel.limited and (!ignore_limits)){
          lvelocity_ = mLevel.limiter->get_limit_speed(x, y, d);
          lpixels_ = mLevel.limiter->get_remaining_pixels(x, y, d);
          //dbg if(lpixels_) std::cerr<<"lpx: "<<(unsigned)lpixels_<<std::endl;
        }
        else if(tmp != mLevel.air)
          (tmp->*collide_func)(col, compl d);
      }
      break;
    case LEFT:
    case RIGHT:
      x = (d == LEFT)?(tlx - 1):(brx + 1);
      for(y = tly; y <= bry; y++){
        tmp = mLevel.get_node(x, y);
        if(tmp == mLevel.limited){
          lvelocity_ = mLevel.limiter->get_limit_speed(x, y, d);
          lpixels_ = mLevel.limiter->get_remaining_pixels(x, y, d);
        }
        else if(tmp != mLevel.air)
          (tmp->*collide_func)(col, compl d);
      }
      break;
    default:
      std::cerr<<"Invalid direction! ";
  }

  if(!col.ok()){
    if(col.lethal()){
      die();
    }
    lpixels_ = 0;
    lvelocity_ = 0;
    return false;
  }
  else{
    set_velocity(col.velocity());
    wipe();

    char dx = 0, dy = 0; //deltas
    switch(d){ //setup deltas
      case UP:
        dy = -1;
        break;
      case DOWN:
        dy = 1;
        break;
      case LEFT:
        dx = -1;
        break;
      case RIGHT:
        dx = 1;
        break;
      default:
        std::cerr<<"Invalid direction! ";
    }

    for(auto &i : nodes){
      i.first += dx;
      i.second += dy;
    }

    // Also update the corners
    tlx += dx;
    tly += dy;
    brx += dx;
    bry += dy;

    project();
    /* debug - node positions
       std::cerr<<"\nNodes: ";
       for(auto n : nodes ){
       std::cerr<<"("<<(unsigned)n.first<<"; "<<(unsigned)n.second<<") ";
       }
       */

    return true;
  }
}


void Collidable::add_node(unsigned char x, unsigned char y)
{
  nodes.push_back(std::make_pair(x, y));
}

void Collidable::clear_nodes()
{
  nodes.clear();
}

void Collidable::project()
{
  for(auto i : nodes){
    mLevel.set_node(i.first, i.second, this);
  }
}

void Collidable::wipe() const
{
  for(auto i : nodes){
    if(mLevel.get_node(i.first, i.second) == this)
      mLevel.set_node(i.first, i.second, mLevel.air);
  }
}


float Collidable::get_velocity() const
{
  return velocity_;
}


void Collidable::set_velocity(float v)
{
  velocity_ = v;
}


// Collision handlers
void Collidable::collide_brick(Collision & col, unsigned char side)
{
  col.deny(false);
}

void Collidable::collide_creature(Collision & col, unsigned char side)
{
  col.deny(false); //default: prevent moving without killing the creature
}

void Collidable::collide_moovie(Collision & col, unsigned char side)
{
  collide_creature(col, side); //moovie is by default "just" creature
}

void Collidable::collide_target(Collision & col, unsigned char side)
{
  collide_brick(col, side); //target brick is also brick
}

void Collidable::collide_machine(Collision & col, unsigned char side)
{
  col.deny(false);
}

void Collidable::push_platform(Collision & col, unsigned char side)
{
  col.approve(); //Not a real collision
}

bool Collidable::collide_ball()
{
  return false;
}

unsigned char Collidable::get_type()
{
  return TYPE_NONE;
}

bool Collidable::stops_blood()
{
  return false;
}

void Collidable::die()
{
  ;
}

void Collidable::teleport(unsigned char x, unsigned char y)
{
  ;
}

}

