/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Program state to load assets
 */

#ifndef STATE_LOAD_H_UM6Y8FDN
#define STATE_LOAD_H_UM6Y8FDN

//#include <thread>
//#include <atomic>
#include <GQE/Core/interfaces/IState.hpp>
#include <GQE/Core/interfaces/TAssetHandler.hpp>
#include <SFML/Graphics/Sprite.hpp>

namespace moovie {

class StateLoad : public GQE::IState
{
  private:
    GQE::TAssetHandler<sf::Texture> & mImageHandler;

    void load();

  protected:
    void HandleCleanup();

  public:
    StateLoad(GQE::IApp & parent);
    virtual ~StateLoad ();

    void DoInit();
    void ReInit();
    void UpdateFixed();
    void UpdateVariable(float);
    void Draw();
};

} /* moovie */

#endif /* end of include guard: STATE_LOAD_H_UM6Y8FDN */
