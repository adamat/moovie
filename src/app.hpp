/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APP_H_BID88CYA
#define APP_H_BID88CYA

#include <GQE/Core/interfaces/IApp.hpp>

namespace moovie {

template <class T>
  class DrawableNode;

class App final : public GQE::IApp
{
  private:
    static App * gApp;
    GQE::TAssetHandler<sf::Texture> *mImageHandler;

  protected:
    virtual void InitAssetHandlers(void);
    virtual void InitScreenFactory(void);
    virtual void HandleCleanup(void);

  public:
    App();
    virtual ~App ();
    static inline App* GetApp(){
      return gApp;
    }

    // easy texture getting
    DrawableNode<sf::Sprite>* make_sprite();
    sf::Texture & get_texture_atlas();
};

}

#endif /* end of include guard: APP_H_BID88CYA */
