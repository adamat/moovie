/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BALL_H_O6C7DASS
#define BALL_H_O6C7DASS

#include "node.hpp"

namespace moovie {

class Ball :
  public Node
{
  private:
    float vx, vy; // vector of movement
    float x, y; // position
    float tx, ty; // temporary

    const unsigned char hwidth = 3, hheight = 3; // position of origin
    const float friction = 0.6f;
    const float y_bounce = 0.6f;
    const float x_bounce = 0.9f;
    const float ground_friction = 150.0f;
    const float gravity = 350.0f;

    bool y_stopped = 0;
    bool x_stopped = 0;
    bool harmless = false; // "get attention" mode
    bool remove = false;
    float delay = 0.0f;

  public:
    Ball(StateLevel & parent, unsigned x_, unsigned y_, float vx_, float vy_);
    virtual ~Ball();

    void on_tick();
    void on_frame(float frame_time);
    void draw(sf::RenderTarget &target, sf::RenderStates states) const;
};

} /* moovie */

#endif /* end of include guard: BALL_H_O6C7DASS */

