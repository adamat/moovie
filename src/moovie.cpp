/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libintl.h>
#include <iostream>
#include "moovie.hpp"
#include "ball.hpp"
//#include "blood.hpp"
#include "constants.hpp"
#include "state_level.hpp"

#define _(STRING) gettext(STRING)

namespace moovie {

thor::FrameAnimation Moovie::up_anim;
thor::FrameAnimation Moovie::down_anim;
thor::FrameAnimation Moovie::right_anim;
thor::FrameAnimation Moovie::left_anim;

void Moovie::setup()
{
  for(int i = 0; i < 9; i++)
    up_anim.addFrame(1.0f, {i * 48, 0, 48, 48});

  for(int i = 0; i < 9; i++)
    left_anim.addFrame(1.0f, {i * 48, 48, 48, 48});

  for(int i = 0; i < 9; i++)
    down_anim.addFrame(1.0f, {i * 48, 96, 48, 48});

  for(int i = 0; i < 9; i++)
    right_anim.addFrame(1.0f, {i * 48, 144, 48, 48});

}

Moovie::Moovie(StateLevel & parent, unsigned char x, unsigned char y) :
  Creature(parent, x, y)
{
  auto sprite = mApp.make_sprite();
  push_back(sprite);

  animator.addAnimation("up", up_anim, sf::milliseconds(250));
  animator.addAnimation("down", down_anim, sf::milliseconds(250));
  animator.addAnimation("right", right_anim, sf::milliseconds(250));
  animator.addAnimation("left", left_anim, sf::milliseconds(250));

  animator.playAnimation("down", true);

  ball_throw_sound_buffer.SetID("assets/sfx/ball_throw.flac");
  ball_throw_sound.setBuffer(ball_throw_sound_buffer.GetAsset());
  ball_catch_sound_buffer.SetID("assets/sfx/ball_catch.flac");
  ball_catch_sound.setBuffer(ball_catch_sound_buffer.GetAsset());

  collide_func = &Collidable::collide_moovie;
}

Moovie::~Moovie()
{
}

unsigned char Moovie::get_type()
{
  return TYPE_MOOVIE;
}

bool Moovie::collide_ball()
{
  // Catch the ball
  ammunition++;
  mLevel.update_balls(ammunition);
  ball_catch_sound.play();
  return true;
}

void Moovie::check_keyboard()
{
  if(not mApp.mWindow.hasFocus())
    return;

  if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left) or
      sf::Keyboard::isKeyPressed(sf::Keyboard::H)){

    float & v = (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::RShift) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt))
      ? hi_speed : lo_speed;

    animator.playAnimation("left", true);

    if(v == hi_speed or (get_direction() == LEFT and
          kb_delay.getElapsedTime() > kb_delay_time)){
      set_direction(LEFT);
      attempt_move(v); // start moving
    }
    else if(get_direction() != LEFT){
      kb_delay.restart();
      set_direction(LEFT);
    }
  }

  else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right) or
      sf::Keyboard::isKeyPressed(sf::Keyboard::L)){

    float & v = (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::RShift) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt))
      ? hi_speed : lo_speed;

    animator.playAnimation("right", true);

    if(v == hi_speed or (get_direction() == RIGHT and
          kb_delay.getElapsedTime() > kb_delay_time)){
      set_direction(RIGHT);
      attempt_move(v); // start moving
    }
    else if(get_direction() != RIGHT){
      kb_delay.restart();
      set_direction(RIGHT);
    }
  }

  else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up) or
      sf::Keyboard::isKeyPressed(sf::Keyboard::K)){

    float & v = (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::RShift) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt))
      ? hi_speed : lo_speed;

    animator.playAnimation("up", true);

    if(v == hi_speed or (get_direction() == UP and
          kb_delay.getElapsedTime() > kb_delay_time)){
      set_direction(UP);
      attempt_move(v); // start moving
    }
    else if(get_direction() != UP){
      kb_delay.restart();
      set_direction(UP);
    }
  }

  else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down) or
      sf::Keyboard::isKeyPressed(sf::Keyboard::J)){

    float & v = (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::RShift) or
        sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt))
      ? hi_speed : lo_speed;

    animator.playAnimation("down", true);

    if(v == hi_speed or (get_direction() == DOWN and
          kb_delay.getElapsedTime() > kb_delay_time)){
      set_direction(DOWN);
      attempt_move(v); // start moving
    }
    else if(get_direction() != DOWN){
      kb_delay.restart();
      set_direction(DOWN);
    }
  }
}

void Moovie::on_tick()
{
}

void Moovie::on_frame(float frame_time)
{

  if(not moving_done() or dead){
    do_moving(frame_time);
    animator.update(sf::seconds(frame_time));
  }

  if(moving_done() and moving_enabled){
    check_keyboard();
  }

  animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(0)));
}

void Moovie::die()
{
  std::cerr<<_("\n***************\n You are dead! \n***************\n");

  mLevel.lose();
  //mLevel.remove_actor(this);
  //pop_back();
  moving_enabled = false;
  Creature::die();
}

void Moovie::disable_moving()
{
  moving_enabled = false;
}

Ball* Moovie::shoot()
{
  Ball *b = nullptr;
  if(ammunition){
    switch(get_direction()){
      case UP:
        if(mLevel.get_node(tlx, tly - 1) == mLevel.air){
          b = new Ball(mLevel, (tlx + 0.5f) * NODE_SIZE,
              tly * NODE_SIZE, 0, -240);
          ammunition--;
          mLevel.update_balls(ammunition);
          ball_throw_sound.play();
        }
        break;
      case DOWN:
        if(mLevel.get_node(tlx, bry + 1) == mLevel.air){
          b = new Ball(mLevel, (tlx + 0.5f) * NODE_SIZE,
              (bry + 1) * NODE_SIZE, 0, 240);
          ammunition--;
          mLevel.update_balls(ammunition);
          ball_throw_sound.play();
        }
        break;
      case RIGHT:
        if(mLevel.get_node(brx + 1, tly) == mLevel.air){
          b = new Ball(mLevel, (brx + 1) * NODE_SIZE,
              (tly + 0.5f) * NODE_SIZE, 240, 0);
          ammunition--;
          mLevel.update_balls(ammunition);
          ball_throw_sound.play();
        }
        break;
      case LEFT:
        if(mLevel.get_node(tlx - 1, tly) == mLevel.air){
          b = new Ball(mLevel, tlx * NODE_SIZE,
              (tly + 0.5f) * NODE_SIZE, -240, 0);
          ammunition--;
          mLevel.update_balls(ammunition);
          ball_throw_sound.play();
        }
        break;
    }
  }
  return b;
}

/*
void Moovie::update_balls()
{
  ballbar->FRect(masdl::Rect(0, 0, 198, 6), masdl::Color(26, 26, 26, 0));
  ball->BlendAlpha(0);
  for(int i = 0; i < ammunition and i < 13; i++)
    ballbar->Blit(*ball, masdl::noRect, masdl::Rect(i * 16, 0));
  ball->BlendAlpha(1);
}
*/

}

