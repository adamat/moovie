/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FADER_H_93OCYPRQ
#define FADER_H_93OCYPRQ

#include <functional>
#include <SFML/Graphics/RectangleShape.hpp>
#include <Thor/Animation.hpp>
#include "node.hpp"

namespace moovie {

/*
 * Fader - a "shield" that has fades in or out the entire scene and calls a
 * function afterwards.
 */

class Fader :
  public INode
{
  private:
    sf::RectangleShape shield;
    thor::Animator<sf::RectangleShape, bool> animator;
    bool fading = false;

    typedef std::function<void(void)> callback_t;
    callback_t callback;

  public:
    Fader(sf::Time fading_time);
    virtual ~Fader();

    void fade_out(callback_t function);
    void fade_in();

    void tick() override;
    void frame(float frame_time) override;
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
};

} /* moovie */

#endif /* end of include guard: FADER_H_93OCYPRQ */
