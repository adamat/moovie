/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLASS_H_GF6ZXJR8
#define GLASS_H_GF6ZXJR8

#include <Thor/Animation.hpp>
#include "brick.hpp"

namespace moovie {

class Glass
: public Brick
{
  private:
    const static unsigned char fragility = 10;
    //how many press tick it takes to break

    static thor::FrameAnimation shine_anim;
    static thor::FrameAnimation break_anim;
    thor::Animator <sf::Sprite, bool> animator;

    GQE::SoundAsset break_sound_buffer;
    sf::Sound break_sound;

    bool second_press = false; // so that top presses don't count twice
    bool pressed = false; //if was pressed during this tick
    int press_count = 0; // -1 -> breaking
    float clock = 0.0f;

    void halve(); //halves the brick's height (during a break)
  protected:
    void press_side();
    void press_top();

  public:
    Glass (StateLevel & parent, unsigned char x, unsigned char y);
    virtual ~Glass ();

    static void setup(); //Configure the animations

    void on_tick();
    void on_frame(float frame_time);
};

}

#endif /* end of include guard: GLASS_H_GF6ZXJR8 */
