/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EMITTER_H_3MCZSFGR
#define EMITTER_H_3MCZSFGR

namespace moovie {

class StateLevel;

class Emitter
{
  protected:
    StateLevel & level;
    unsigned char grp; //the control group

  protected:
    Emitter(StateLevel & parent, unsigned char group);
    virtual ~Emitter();

    void emit();
};

} /* moovie */

#endif /* end of include guard: EMITTER_H_3MCZSFGR */

