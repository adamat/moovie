/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits>
#include <sstream>
#include <system_error>
#include <string>
#include <cerrno>
#include <libintl.h>

#include "state_level.hpp"
#include "moovie.hpp"
#include "solid.hpp"
#include "brick.hpp"
#include "base.hpp"
#include "glass.hpp"
#include "lift.hpp"
#include "receiver.hpp"
#include "button.hpp"
#include "teleport.hpp"
#include "sensor.hpp"
#include "ball.hpp"
#include "v_door.hpp"
#include "pusher.hpp"
#include "h_door.hpp"

#define _(STRING) gettext(STRING)

namespace moovie {

void StateLevel::load_level() throw(std::exception)
{
  std::istringstream in(leveldef);
//  if(!check_validity(in)) return false;

  /* Temporary storage to allow load-time sorting into layers
   * 0 - background images
   * 1 - Lift, Platform
   * 2 - standard
   * 3 - Pusher
   * 4 - Moovie, creatures
   * 5 - Teleport, HorizontalDoor, VerticalDoor
   * 6 - foreground images
   */

  std::list<INode*> layer[6];

  char c = 0;
  unsigned v1 = 0, v2, v3, v4, v5, v6, v7;
  unsigned short b = 0;
  std::string s;

  // Set background
  in >> s;
  if(not background.loadFromFile(s)){
    char estr[512];
    snprintf(estr, 512, _("Could not load background image %s"), s.c_str());
    perror(estr);
  }

  auto bg = new DrawableNode<sf::Sprite> (background);
  actors.push_back(bg);

  bool has_moovie = 0,
       has_target = 0,
       has_base = 0;

  auto load_control_groups = [&](Receiver* r)
  {
    unsigned g = 1;
    while(g){
      in >> g;
      if(in.fail()) break;
      add_receiver(r, g);
    }
  };

  INode *tmp;
  Machine *rec;
  Teleport *tel;
  Sensor *sen;
  VerticalDoor *vdoor;

  while(!in.eof() and c != '%'){
    c = 0;
    in >> c;
    switch(c){
      // Comment
      case '#':
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        break;

      // Solid block
      case '.':
        in >> v1 >> v2;
        set_node(v1, v2, solid);
        break;

      // Moovie
      case 'M':
        if(has_moovie) throw std::runtime_error(_("Multiple Moovies"));
        in >> v1 >> v2;
        moovie = new Moovie(*this, v1, v2);
        layer[4].push_back(moovie);
        has_moovie = true;
        break;

      // Target brick
      case 'T':
        if(has_target) throw std::runtime_error(_("Multiple target bricks"));
        in >> v1 >> v2;
        tmp = new Target(*this, v1, v2);
        layer[2].push_back(tmp);
        has_target = true;
        break;

      // Base
      case 'B':
        if(has_base) throw std::runtime_error(_("Multiple bases"));
        in >> v1 >> v2;
        tmp = new Base(*this, v1, v2);
        layer[2].push_back(tmp);
        has_base = true;
        break;

      // Brick (third value is texture variant)
      case 'b':
        in >> v1 >> v2 >> b;
        if((b > 3 and b < 10) or b > 13)
          b -= (b % 10);
        tmp = new Brick(*this, v1, v2, b);
        layer[2].push_back(tmp);
        break;

      // Glass brick
      case 'g':
        in >> v1 >> v2;
        tmp = new Glass(*this, v1, v2);
        layer[2].push_back(tmp);
        break;

      // Vertical lift
      case 'v':
        in >> v1 >> v2 >> v3 >> v4 >> b;
        b = b > 1 ? 0 : b;
        rec = new Lift(*this, v1, v2, v3, sf::milliseconds(v4), b);
        load_control_groups(rec);
        layer[1].push_back(rec);
        break;

      // Horisontal lift
      case 'h':
        in >> v1 >> v2 >> v3 >> v4 >> b;
        b = b > 1 ? 0 : b;
        rec = new Platform(*this, v1, v2, v3, sf::milliseconds(v4), b);
        load_control_groups(rec);
        layer[1].push_back(rec);
        break;

      // Button on the left side
      case ']':
        in >> v1 >> v2 >> b >> v3;
        b = b > 1 ? 0 : b;
        tmp = new Button(*this, v1, v2, RIGHT, b, v3);
        layer[2].push_back(tmp);
        break;

      // Button on the right side
      case '[':
        in >> v1 >> v2 >> b >> v3;
        b = b > 1 ? 0 : b;
        tmp = new Button(*this, v1, v2, LEFT, b, v3);
        layer[2].push_back(tmp);
        break;

      // Button on the bottom side
      case '_':
        in >> v1 >> v2 >> b >> v3;
        b = b > 1 ? 0 : b;
        tmp = new Button(*this, v1, v2, UP, b, v3);
        layer[2].push_back(tmp);
        break;

      // Pair of teleports
      case 't':
        in >> v1 >> v2 >> v3 >> v4 >> v5 >> b;
        b = b > 1 ? 0 : b;
        tel = new Teleport(*this, v1, v2, v3, v4, sf::milliseconds(v5), b);
        in >> v1;
        tel->add_sprite(v1, b);
        load_control_groups(tel);
        layer[5].push_back(tel);
        break;

      // Sensor
      case 's':
        in >> v1 >> v2 >> v3 >> v4;
        sen = new Sensor(*this, v1, v2, v3, v4);
        sensors.push_back(sen);
        break;

      // Ball
      case 'o':
        in >> v1 >> v2;
        tmp = new Ball(*this, v1 * NODE_SIZE + NODE_SIZE/2,
            (v2 + 1) * NODE_SIZE - 3, 0, 0);
        layer[2].push_back(tmp);
        break;

      // Vertical door
      case '|':
        in >> v1 >> v2 >> v3 >> v4 >> b;
        b = b > 1 ? 0 : b;
        vdoor = new VerticalDoor(*this, v1, v2, sf::milliseconds(v3), v4, b);
        load_control_groups(vdoor);
        layer[5].push_back(vdoor);
        break;

      // Pusher
      case 'p':
        in >> v1 >> v2 >> v3 >> b;
        b = b > 1 ? 0 : b;
        rec = new Pusher(*this, v1, v2, v3, b);
        load_control_groups(rec);
        layer[3].push_back(rec);
        break;

      // Horisontal door
      case 'd':
        in >> v1 >> v2 >> v3 >> v4 >> v5 >> b;
        b = b > 1 ? 0 : b;
        rec = new HorizontalDoor(*this, v1, v2, v3, sf::milliseconds(v4), v5,
            b);
        load_control_groups(rec);
        layer[5].push_back(rec);
        break;

      /*
       * This tragedy will NEVER be in Moovie!
      // Decor
      case '*':
        // Ninth bit in v7 is the Slowness bit
        in >> s >> v1 >> v2 >> v3 >> v4 >> v5 >> v6 >> v7;
        tmp = new Decor(s.c_str(), v1, v2, v3, v4, v5, v6, v7 & 0xFF, v7 & 256);
        actors.push_back(tmp);
        break;
      */

      case 0:
        break;

      // Unknown
      default:
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        WLOG() << "Unknown token '" << c << "'!" << std::endl;
        break;
    }
  }

  if(!has_moovie) throw std::runtime_error(_("No Moovie"));
  if(!has_target) throw std::runtime_error(_("No target brick"));
  if(!has_base) throw std::runtime_error(_("No base"));

  for(auto & l : layer){
    actors.insert(actors.end(), l.begin(), l.end());
  }
}

}

