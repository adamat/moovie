/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "creature.hpp"
#include "state_level.hpp"
#include "blood.hpp"

namespace moovie {

thor::FrameAnimation Creature::death_anim;
void Creature::setup()
{
  for(int i = 0; i < 9; i++)
    death_anim.addFrame(1.0f, {432 + i * 48, 0, 48, 48});
  death_anim.addFrame(4.0f, {0, 511, 1, 1});
}

Creature::Creature(StateLevel & parent, unsigned char x, unsigned char y) :
  Movable(parent)
{
  init_xy(x, y, 1, 1);
  ignore_limits = true;
  //Assign collision function
  collide_func = &Collidable::collide_creature;
  bloodsplash_sound_buffer.SetID("assets/sfx/menu_enter.flac");
  bloodsplash_sound.setBuffer(bloodsplash_sound_buffer.GetAsset());
  animator.addAnimation("death", thor::refAnimation(death_anim),
      sf::milliseconds(500));
}

Creature::~Creature()
{
  ;
}

void Creature::die()
{
  wipe();
  dead = true;

  // Bloodshed!
  setPosition(0, 0);
  static_cast<DrawableNode<sf::Sprite>*>(at(0))->setPosition(tlx * NODE_SIZE,
      tly * NODE_SIZE);
  push_back(new Blood(mLevel, (tlx+1)*NODE_SIZE, (tly+1)*NODE_SIZE, 30, -200));
  push_back(new Blood(mLevel, (tlx+1)*NODE_SIZE, (tly+1)*NODE_SIZE, 100, -100));
  push_back(new Blood(mLevel, (tlx+1)*NODE_SIZE, (tly+1)*NODE_SIZE, 200 , 0));
  push_back(new Blood(mLevel, (tlx+1)*NODE_SIZE, (tly+1)*NODE_SIZE, 100, 100));
  push_back(new Blood(mLevel, (tlx+1)*NODE_SIZE, (tly+1)*NODE_SIZE, -30, 200));
  push_back(new Blood(mLevel, (tlx+1)*NODE_SIZE, (tly+1)*NODE_SIZE, -100, 100));
  push_back(new Blood(mLevel, (tlx+1)*NODE_SIZE, (tly+1)*NODE_SIZE, -200, 100));
  push_back(new Blood(mLevel, (tlx+1)*NODE_SIZE, (tly+1)*NODE_SIZE,-100, -100));
  bloodsplash_sound.play();
  animator.playAnimation("death");
}

void Creature::collide_brick(Collision & col, unsigned char side)
{
  side &= 3;
  switch(side){
    case UP:
      col.approve();
      //If nothing else prevents the brick from falling, I must die
      col.kill(this);
      break;
    default:
      col.deny();
  }
}

}

