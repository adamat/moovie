/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <GQE/Core/states/SplashState.hpp>
#include <ctime>
#include <GQE/Core/Core_types.hpp>
#include "app.hpp"
#include "constants.hpp"
#include "state_load.hpp"
#include "state_menu.hpp"
#include "state_level.hpp"

namespace moovie {

App* App::gApp = nullptr;

App::App() :
  GQE::IApp("Moovie")
{
  mProperties.Add<GQE::Uint32>("uWindowWidth", WIN_WIDTH);
  mProperties.Add<GQE::Uint32>("uWindowHeight", WIN_HEIGHT);
  mProperties.Add<GQE::Uint32>("uWindowDepth", 32);
  mWindowStyle = sf::Style::Close;
  SetMaxUpdates(1);
  SetUpdateRate(UPDATE_RATE);

  srand(time(NULL));

  gApp = this;
}

void App::HandleCleanup(void)
{
}

void App::InitAssetHandlers(void)
{
  mImageHandler = &(mAssetManager.GetHandler<sf::Texture>());
}

void App::InitScreenFactory(void)
{
  mStateManager.AddActiveState(new(std::nothrow) StateLoad(*this));
  mStateManager.AddInactiveState(new(std::nothrow) StateMenu(*this));
  mStatManager.SetShow(true);
}

DrawableNode<sf::Sprite>* App::make_sprite()
{
  auto sp = new DrawableNode<sf::Sprite>(*mImageHandler->GetReference("atlas"));
  return sp;
}

sf::Texture & App::get_texture_atlas()
{
  return *mImageHandler->GetReference("atlas");
}

App::~App()
{
  if(gApp == this)
    gApp = nullptr;
}

}
