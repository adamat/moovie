/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MOOVIE_H_FKTLAXKD
#define MOOVIE_H_FKTLAXKD

#include <Thor/Animation.hpp>
#include <GQE/Core/assets/SoundAsset.hpp>
#include "creature.hpp"

namespace moovie {

class Ball;

class Moovie :
  public Creature
{
  private:
    float hi_speed = 180.0f, lo_speed = 90.0f; //velocities
    unsigned char ammunition = 0; // number of balls (two if male)
    bool moving_enabled = true;

    sf::Clock kb_delay;
    const sf::Time kb_delay_time = sf::milliseconds(80);
    void check_keyboard();

    static thor::FrameAnimation up_anim;
    static thor::FrameAnimation down_anim;
    static thor::FrameAnimation right_anim;
    static thor::FrameAnimation left_anim;

    GQE::SoundAsset ball_throw_sound_buffer;
    sf::Sound ball_throw_sound;
    GQE::SoundAsset ball_catch_sound_buffer;
    sf::Sound ball_catch_sound;

  public:
    Moovie (StateLevel & parent, unsigned char x, unsigned char y);
    virtual ~Moovie ();

    static void setup();

    unsigned char get_type();

    bool collide_ball();

    void on_tick();
    void on_frame(float frame_time) override;
    void die();
    void disable_moving();
    Ball* shoot();
    //void draw();
};

}

#endif /* end of include guard: MOOVIE_H_FKTLAXKD */
