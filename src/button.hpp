/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BUTTON_H_IAL9WFN8
#define BUTTON_H_IAL9WFN8

#include <Thor/Animation.hpp>
#include <GQE/Core/assets/SoundAsset.hpp>
#include "emitter.hpp"
#include "collidable.hpp"

namespace moovie {

class Button :
  protected Emitter,
  public Collidable
{
  private:
    unsigned char dir_; //From where the push goes
    char fx, fy, sx, sy; // Pusher nodes
    bool second_press = false;
    bool pushed = false;
    bool was_pushed = false;

    void push(bool force = false);

    static thor::FrameAnimation anim_push_right[2];
    static thor::FrameAnimation anim_push_left[2];
    static thor::FrameAnimation anim_push_horisontal[2];
    static thor::FrameAnimation anim_release_right[2];
    static thor::FrameAnimation anim_release_left[2];
    static thor::FrameAnimation anim_release_horisontal[2];
    thor::Animator <sf::Sprite, int> animator;
    // 1 - push
    // 2 - release

    GQE::SoundAsset push_sound_buffer;
    GQE::SoundAsset release_sound_buffer;
    sf::Sound push_sound;
    sf::Sound release_sound;

  public:
    Button (StateLevel & parent, unsigned char x, unsigned char y,
        unsigned char dir, unsigned char asset, unsigned char group);
    virtual ~Button ();

    static void setup();

    void collide_moovie(Collision & col, unsigned char side);
    void collide_brick(Collision & col, unsigned char side);
    bool stops_blood();
    bool collide_ball();

    void on_tick();
    void on_frame(float frame_time);
};

} /* moovie */

#endif /* end of include guard: BUTTON_H_IAL9WFN8 */
