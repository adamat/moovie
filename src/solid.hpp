/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//Class for the level "walls"

#ifndef SOLID_H_XBK9SJR8
#define SOLID_H_XBK9SJR8

#include "collidable.hpp"

namespace moovie {

class Solid : public Collidable
{
  public:
    Solid(StateLevel & parent);
    virtual ~Solid();
    bool stops_blood();
    // All other collisions are in their default state - always blocking
};

}

#endif /* end of include guard: SOLID_H_XBK9SJR8 */
