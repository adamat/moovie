/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/******************
  Moovie - Collision class
 ******************/

#ifndef COLLISION_H_M0PZDT62
#define COLLISION_H_M0PZDT62

#include <set>

namespace moovie {

class Creature;

class Collision{
  private:
    bool approved = true;
    bool must_die = false; //implies denial; 'approved' may remain true -> kill
    float velocity_ = 24e3f; // in pixels per second
    std::set<Creature* > to_be_killed;

  public:
    void approve(); // The object doesn't block (possibly by its own death)
    void absolute_approve(); // Sets can_move to true; for blocks
    void deny(bool lethal = false); // Don't move. If the collision is to happen, the collider will be killed

    void kill(Creature *cr); //Add to kill list (creatures add themselves)
    void set_max_velocity(float v); //the lowest one counts

    bool ok(); //checks, whether the collider may move. Kills creatures, if needed
    bool lethal(); //whether the collider have to die
    float velocity(); //the lowest velocity; at that v the move happens
};

}

#endif /* end of include guard: COLLISION_H_M0PZDT62 */

