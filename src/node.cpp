/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "node.hpp"
#include "state_level.hpp"

namespace moovie {

INode::INode()
{
}

INode::~INode()
{
}

Node::Node() :
  mApp(*moovie::App::GetApp()),
  mLevel(*(StateLevel*)NULL) // I HATE MYSELF SO MUCH
{
  if(!moovie::App::GetApp())
    std::cerr<<"NULL App!";
}

Node::Node(StateLevel & parent) :
  mApp(*moovie::App::GetApp()),
  mLevel(parent)
{
  if(!moovie::App::GetApp())
    std::cerr<<"NULL App!";
}

void Node::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  states.transform *= getTransform();
  on_draw(target, states);
  for(auto i : *this){
    i->draw(target, states);
  }
}

void Node::frame(float frame_time)
{
  on_frame(frame_time);
  for(auto i : *this){
    i->frame(frame_time);
  }
}

void Node::tick()
{
  on_tick();
  for(auto i : *this){
    i->tick();
  }
}

void Node::on_draw(sf::RenderTarget &target, sf::RenderStates states) const
{
}

void Node::on_frame(float frame_time)
{
}

void Node::on_tick()
{
}

Node::~Node()
{
  for(auto i : *this){
    delete i;
  }
}


}
