/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/******************
  Moovie - Collidable class
 ******************/

#ifndef COLLIDABLE_H_1439T1EH
#define COLLIDABLE_H_1439T1EH

#include <utility>
#include <list>
#include "collision.hpp"
#include "node.hpp"

namespace moovie {

// Types of sensor-detectable actors
enum {
  TYPE_NONE = 0, //Not detectable
  TYPE_BRICK = 1, //All types of bricks
  TYPE_ENEMY = 2,
  TYPE_MOOVIE = 4,
};

class Collidable : public Node
{
  private:
    std::list< std::pair<unsigned char, unsigned char> > nodes;
    // Which nodes are collidable
    // The reason why each node instead of range is used is that moving is much
    // simpler that way and also that some collidables may not be rectangular
    float velocity_ = 0; //Passed as max. collision speed

  protected:
    // However, for rectangular objects, here are corners:
    unsigned char tlx = 0, brx = 0; //Top-left and bottom-right X
    unsigned char tly = 0, bry = 0; //Top-left and bottom-right Y

    void init_xy(unsigned char x, unsigned char y,
        unsigned char w, unsigned char h); //helper function

    // Limiter stuff
    bool ignore_limits = false; // For creatures
    float lvelocity_ = 0; //Limited velocity
    float lpixels_ = 0; //Pixels to continue before switching to limit

    void project(); // Projects the nodes onto Scene's node grid
    void wipe() const; // Wipes the nodes from Scene's node grid
    virtual bool attempt_step(unsigned char d); //try to move in the direction
    // Virtual because of horisontal platform
    void (Collidable::*collide_func)(Collision &, unsigned char) = 0; //depends on who's colliding
    void add_node(unsigned char x, unsigned char y);
    void clear_nodes();

    void set_velocity(float v);
    float get_velocity() const;

  public:
    Collidable(StateLevel & parent);
    ~Collidable();

    // Collision handlers for every object type
    virtual void collide_brick(Collision & col, unsigned char side);
    virtual void collide_target(Collision & col, unsigned char side);
    virtual void collide_moovie(Collision & col, unsigned char side);
    virtual void collide_creature(Collision & col, unsigned char side);
    virtual void collide_machine(Collision & col, unsigned char side);
    virtual void push_platform(Collision & col, unsigned char side);
    virtual bool collide_ball(); // Returns true, if the ball is to be removed

    virtual unsigned char get_type(); //Only for sensors
    virtual bool stops_blood();
    virtual void die();
    virtual void teleport(unsigned char x, unsigned char y);
};

}

#endif /* end of include guard: COLLIDABLE_H_1439T1EH */

