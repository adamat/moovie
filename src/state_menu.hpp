/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014-2016 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Program state showing main menu
 */

#ifndef STATE_MENU_H_AGBXQNYW
#define STATE_MENU_H_AGBXQNYW

#include <GQE/Core/interfaces/IState.hpp>
#include <GQE/Core/assets/FontAsset.hpp>
#include <GQE/Core/assets/SoundAsset.hpp>
#include "app.hpp"
#include "fader.hpp"
#include "move_animation.hpp"

namespace moovie {

struct MenuEntry
{
  enum Type {
    LEVEL,
    QUIT
  } type;
  int data;
  std::string label;
};

class MyRectangleShape : public sf::RectangleShape
{
  public:
    inline void setColor(const sf::Color & c) {setFillColor(c);}
};

class StateMenu : public GQE::IState
{
  private:
    moovie::App & mApp;
    GQE::FontAsset font_asset;
    Fader fader;

    sf::RectangleShape bg;
    Node menuitems;
    std::vector<MenuEntry> entries;
    int n_items = 0;
    int selected = 0;

    sf::Text arrow_left, arrow_right;
    thor::ColorGradient grad_focus, grad_unfocus;
    thor::ColorAnimation *anim_focus, *anim_unfocus;
    MoveAnimation anim_move;
    thor::Animator<Node, int> *menuanim;

    GQE::SoundAsset move_sound_buffer;
    GQE::SoundAsset enter_sound_buffer;
    sf::Sound move_sound;
    sf::Sound enter_sound;

  protected:
    void HandleCleanup();

  public:
    StateMenu(moovie::App & parent);
    virtual ~StateMenu ();

    void DoInit();
    void ReInit();
    void HandleEvents(sf::Event theEvent);
    void UpdateFixed();
    void UpdateVariable(float frame_time);
    void Draw();
};

} /* namespace moovie */

#endif /* end of include guard: STATE_MENU_H_AGBXQNYW */

