/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "glass.hpp"
#include "state_level.hpp"

namespace moovie {

thor::FrameAnimation Glass::shine_anim;
thor::FrameAnimation Glass::break_anim;

void Glass::setup()
{
  shine_anim.addFrame(180.0f, {0, 192, 48, 48});
  for(int i = 1; i <= 5; i++)
    shine_anim.addFrame(1.0f, {i * 48, 192, 48, 48});

  break_anim.addFrame(15.0f, {0, 240, 48, 48});
  for(int i = 1; i <= 15; i++)
    break_anim.addFrame(1.0f, {i * 48, 240, 48, 48});
}

Glass::Glass(StateLevel & parent, unsigned char x, unsigned char y):
  Brick(parent, x, y)
{
  auto sprite = mApp.make_sprite();
  push_back(sprite);
  animator.addAnimation(0, thor::refAnimation(shine_anim), sf::seconds(6.5));
  animator.addAnimation(1, thor::refAnimation(break_anim), sf::seconds(1));
  animator.playAnimation(0, true);

  // Randomise the shines
  animator.update(sf::milliseconds(rand() % 6200));

  impact_sound_buffer.SetID("assets/sfx/glass_fall.flac");
  impact_sound.setBuffer(impact_sound_buffer.GetAsset());
  break_sound_buffer.SetID("assets/sfx/glass_break.flac");
  break_sound.setBuffer(break_sound_buffer.GetAsset());
}

Glass::~Glass()
{
  ;
}

void Glass::on_tick()
{
  second_press = false;
  if(press_count >= 0){
    if(pressed){
      pressed = false;
      if(++press_count >= fragility){
        press_count = -1;
        animator.playAnimation(1);
        break_sound.play();
      }
    }
    else
      press_count = 0; //pressing has stopped
  }

  if(press_count == -1){
    clock += 1.0 / (float) UPDATE_RATE;
    if(clock >= 1.0f){
      wipe();
      for(INode *i : *this)
        delete i; // Delete all child nodes
      this->clear();
      press_count = -2;
    }
    else if(clock >= 0.7f){
      halve();
    }
  }
  else if(press_count == -2 and
      break_sound.getStatus() == sf::SoundSource::Status::Stopped){
    mLevel.remove_actor(this);
  }
}

void Glass::on_frame(float frame_time)
{
  if(press_count != -2){
    animator.update(sf::seconds(frame_time));
    animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(0)));

    Brick::on_frame(frame_time);
  }
}

void Glass::press_side()
{
  pressed = true;
}


void Glass::press_top()
{
  if(second_press == false and press_count >= 0){
    second_press = true;
    pressed = true;
  }
}

void Glass::halve()
{
  wipe();
  clear_nodes();
  add_node(tlx, bry);
  add_node(brx, bry);
  tly--;
  project();
}

}

