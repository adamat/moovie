/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "h_door.hpp"
#include "state_level.hpp"
#include "constants.hpp"

namespace moovie {

HorizontalDoor::HorizontalDoor(StateLevel & parent, unsigned char x,
    unsigned char y, unsigned char tox, sf::Time time_to_close, bool closed,
    unsigned char asset) :
  Machine(parent, sf::milliseconds(0))
{
  auto s = mApp.make_sprite();
  to_left = (tox > x) ? false : true;
  sf::FloatRect bodysrc;
  sf::IntRect backbodysrc;


  // H-Door's sprites are bit different - the "head" part is overlapping on
  // both sides partially
  unsigned asset_y = (asset) ? 312 : 288;
  if(not to_left){
    bodysrc = {816, asset_y, 24, 24};
    backbodysrc = {840, asset_y, 24, 24};
    s->setOrigin(12, 0);
    x--;
  }
  else{
    bodysrc = {792, asset_y, 24, 24};
    backbodysrc = {768, asset_y, 24, 24};
    s->setOrigin(12, 0);
    x++;
  }

  s->setTextureRect(backbodysrc);
  body = new RepeatedQuad(&mApp.get_texture_atlas(), bodysrc, !to_left);
  push_back(body);
  push_back(s);

  if(not to_left){ //This just works, 'kay?
    body->move(NODE_SIZE, 0);
    s->move(NODE_SIZE, 0);
  }

  length = (to_left)? x - tox : tox - x;

  if(closed){
    init_xy(tox, y, 0, 0);
    for(unsigned char i = 1; i <= length; i++){
      mLevel.set_node(tlx + (to_left ? i : -i), tly, this);
      body->change_size(NODE_SIZE);
    }
    will_back = true;
    length_counter = length;
    timeout = time_to_close.asSeconds() * NODE_SIZE;
  }
  else{
    init_xy(x, y, 0, 0);
    timeout = 0;
  }
}


void HorizontalDoor::activate()
{
  if(not moving and not will_back){
    if(try_closing)
      will_back = true;
    will_move = true;
    force = true;
  }
  else if(not moving and will_back){
    will_move = true;
  }
}


void HorizontalDoor::on_tick()
{
  if((will_move or try_closing) and not will_back){
    if(not force and delay < timeout) delay++;
    else{
      set_direction(to_left ? LEFT : RIGHT);
      moving = attempt_move(velocity);
      if(moving){
        length_counter++;

        unsigned char px = tlx + (to_left ? 1 : -1);
        mLevel.set_node(px, tly, this);
        will_move = false;
        try_closing = false;
      }
      force = false;
      delay = 0;
    }
  }
  else if(will_move and will_back){
    back = true;
    will_back = false;
    will_move = false;
    try_closing = false;
    delay = 0;
    force = false;
    set_direction(to_left ? RIGHT : LEFT);
    pending_collision = true;
    moving = attempt_move(velocity);
    pending_collision = false;
    length_counter--;
  }
}

void HorizontalDoor::on_frame(float frame_time)
{
  Machine::on_frame(frame_time);

  if(moving and not back){
    float delta = velocity * frame_time;
    bool quad_done= body->change_size(delta);
    if(quad_done){
      if(length_counter < length and attempt_move(velocity)){
        length_counter++;

        unsigned char px = tlx + (to_left ? 1 : -1);
        mLevel.set_node(px, tly, this);
      }
      else{
        moving = false;
        if(timeout > 0 and length_counter < length)
          try_closing = true; // If closing automatically, keep trying
        else
          will_back = true;
      }
    }
  }
  else if(moving and back){
    float delta = velocity * frame_time;
    bool quad_done = body->change_size(-delta);
    if(quad_done){
      if(length_counter > 0){
      pending_collision = true;
      attempt_move(velocity);
      pending_collision = false;
      length_counter--;
      }
      else{
        back = false;
        moving = false;
        if(timeout > 0)
          will_move = true;
      }
    }
  }
}

HorizontalDoor::~HorizontalDoor()
{
}

void HorizontalDoor::collide_machine(Collision & col, unsigned char side)
{
  if(pending_collision) col.approve();
  else col.deny();
}

} /* moovie */

