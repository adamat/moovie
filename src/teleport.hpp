/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TELEPORT_H_KOALKLZC
#define TELEPORT_H_KOALKLZC

#include <Thor/Animation.hpp>
#include <GQE/Core/assets/SoundAsset.hpp>
#include "node.hpp"
#include "timer.hpp"
#include "receiver.hpp"

namespace moovie {

class Teleport :
  public Node,
  public Receiver
{
  private:
    Timer timer;
    unsigned char x[2],
                  y[2]; // coordinates of the two ports

    /* m = machine (body), f = flash */
    static thor::FrameAnimation idle_m_anim[2];
    static thor::FrameAnimation busy_m_anim[2];
    static thor::FrameAnimation idle_f_anim;
    static thor::FrameAnimation prep_f_anim;
    static thor::FrameAnimation flash_f_anim;
    static thor::FrameAnimation after_f_anim;

    thor::Animator <sf::Sprite, bool> m_animator;
    thor::Animator <sf::Sprite, unsigned char> f_animator;
    unsigned char busy = 0;
      // 0 - Idle
      // 1 - To be started
      // 2 - Preparation
      // 3 - Flash
      // 4 - After-flash effect at first port
      // 5 - After-flash effect at second port

    sf::Sprite *m[2] = {nullptr, nullptr};

    GQE::SoundAsset aftereffect_sound_buffer;
    sf::Sound aftereffect_sound;

  public:
    Teleport (StateLevel & parent, unsigned char x1, unsigned char y1,
        unsigned char x2, unsigned char y2, sf::Time timeout,
        unsigned char asset);
    virtual ~Teleport ();
    static void setup();

    void add_sprite(unsigned char port, unsigned char asset);
    void activate();
    void on_tick();
    void on_frame(float frame_time);
};

} /* moovie */

#endif /* end of include guard: TELEPORT_H_KOALKLZC */
