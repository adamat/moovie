/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libintl.h>
#include <iostream>

#include "state_menu.hpp"
#include "state_level.hpp"
#include "constants.hpp"

#define _(STRING) gettext(STRING)

namespace moovie {

const int MENUITEM_HEIGHT = 72;

std::wstring stows(const char* s)
{
  int len = strlen(s);
  std::wstring tmp(len, L' ');
  tmp.resize(std::mbstowcs(&tmp[0], s, len));
  return tmp;
}

StateMenu::StateMenu(moovie::App & parent) :
  GQE::IState("menu", parent),
  mApp(parent),
  bg({WIN_WIDTH, WIN_HEIGHT}),
  font_asset("assets/fonts/default"),
  arrow_left(">", font_asset.GetAsset(), 40),
  arrow_right("<", font_asset.GetAsset(), 40),
  fader(sf::milliseconds(300)),
  anim_move({256, 220}, {0, 0}),
  menuitems()
{
  bg.setFillColor({20, 20, 20, 255});
  arrow_left.setPosition(224, 218);
  arrow_left.setColor({100, 10, 0, 255});
  arrow_right.setPosition(522, 218);
  arrow_right.setColor({100, 10, 0, 255});

  grad_focus[0.0] = {255, 20, 0, 128};
  grad_focus[1.0] = {255, 20, 0, 255};
  grad_unfocus[0.0] = {255, 20, 0, 255};
  grad_unfocus[1.0] = {255, 20, 0, 128};
  anim_focus = new thor::ColorAnimation(grad_focus);
  anim_unfocus = new thor::ColorAnimation(grad_unfocus);

  for(int i = 1; i < 8; i++){
    const char *src = _("Level %d");
    int len = strlen(src) + 3;
    char * label = new char[len];
    snprintf(label, len, src, i);
    entries.push_back({MenuEntry::Type::LEVEL, i, label});
    delete label;
  }
  entries.push_back({MenuEntry::Type::QUIT, 0, _("Quit")});
  for(int i = 0; i < entries.size(); i++){
    auto node = new Node;
    auto button = new DrawableNode<MyRectangleShape>;
    button->setSize({256, 48});
    button->setFillColor({255, 20, 0, 128});
    button->setPosition(256, MENUITEM_HEIGHT * i);
    auto label = new DrawableNode<sf::Text>;
    label->setFont(font_asset.GetAsset());
    label->setCharacterSize(20);
    label->setString(stows(entries[i].label.c_str()));
    label->setPosition(int(384 - label->getLocalBounds().width/2),
        int(MENUITEM_HEIGHT * i + 20 - label->getLocalBounds().height/2));
    auto anim = new AnimatorNode<MyRectangleShape, int>(*button);
    anim->addAnimation(0, thor::refAnimation(*anim_focus), sf::seconds(0.2));
    anim->addAnimation(1, thor::refAnimation(*anim_unfocus), sf::seconds(0.3));
    node->push_back(button);
    node->push_back(label);
    node->push_back(anim);
    n_items++;
    menuitems.push_back(node);
  }

  auto anim = new AnimatorNode<Node, int>(menuitems);
  menuanim = anim;
  menuanim->addAnimation(0, thor::refAnimation(anim_move), sf::seconds(0.15));
  menuanim->addAnimation(1, thor::refAnimation(anim_move), sf::seconds(0.5));
  menuanim->addAnimation(2, thor::refAnimation(anim_move), sf::seconds(0.25));
  menuitems.push_back(anim);

  static_cast<AnimatorNode<MyRectangleShape, int>* >
    (static_cast<Node*>(menuitems[0])->at(2))->playAnimation(0);

  move_sound_buffer.SetID("assets/sfx/menu_move.flac");
  enter_sound_buffer.SetID("assets/sfx/menu_enter.flac");
  move_sound.setBuffer(move_sound_buffer.GetAsset());
  enter_sound.setBuffer(enter_sound_buffer.GetAsset());
}

void StateMenu::DoInit()
{
  IState::DoInit();
  ReInit();
}

void StateMenu::ReInit()
{
  fader.fade_in();
  anim_move.apply(menuitems);
  anim_move.set_translation({-256, 0});
  menuanim->playAnimation(2);
}

void StateMenu::HandleEvents(sf::Event theEvent)
{
  if (menuanim->isPlayingAnimation())
    return;

  switch(theEvent.type){
    case sf::Event::KeyPressed:
      switch(theEvent.key.code){
        case sf::Keyboard::Down:
        case sf::Keyboard::J:
          static_cast<AnimatorNode<MyRectangleShape, int>* >
            (static_cast<Node*>(menuitems[selected])->at(2))->playAnimation(1);
          selected++;
          if (selected == n_items) {
            anim_move.set_translation({0, --selected * MENUITEM_HEIGHT});
            selected = 0;
          } else {
            anim_move.set_translation({0, -MENUITEM_HEIGHT});
          }
          menuanim->playAnimation(0);
          static_cast<AnimatorNode<MyRectangleShape, int>* >
            (static_cast<Node*>(menuitems[selected])->at(2))->playAnimation(0);
          move_sound.play();
          break;
        case sf::Keyboard::Up:
        case sf::Keyboard::K:
          static_cast<AnimatorNode<MyRectangleShape, int>* >
            (static_cast<Node*>(menuitems[selected])->at(2))->playAnimation(1);
          selected--;
          if (selected == -1) {
            selected = n_items - 1;
            anim_move.set_translation({0, -selected * MENUITEM_HEIGHT});
          } else {
            anim_move.set_translation({0, MENUITEM_HEIGHT});
          }
          menuanim->playAnimation(0);
          static_cast<AnimatorNode<MyRectangleShape, int>* >
            (static_cast<Node*>(menuitems[selected])->at(2))->playAnimation(0);
          move_sound.play();
          break;
        case sf::Keyboard::Q:
          mApp.mStateManager.RemoveActiveState();
        case sf::Keyboard::Return:
          enter_sound.play();
          switch (entries[selected].type) {
            case MenuEntry::Type::QUIT:
              mApp.mStateManager.RemoveActiveState();
              break;
            case MenuEntry::Type::LEVEL:
              anim_move.set_translation({256, 0});
              menuanim->playAnimation(1);
              fader.fade_out([&](){
                  try{
                    mApp.mStateManager.AddInactiveState(new StateLevel(mApp,
                          "levels.lvl", entries[selected].data));
                    mApp.mStateManager.DropActiveState();
                  } catch (std::exception & e){
                    std::cerr << _("Failed to load level") << ": " << e.what()
                        << std::endl;
                    fader.fade_in();
                  }
                  });
              break;
          }
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
}

void StateMenu::UpdateFixed()
{
}

void StateMenu::UpdateVariable(float frame_time)
{
  fader.frame(frame_time);
  bool moving = menuanim->isPlayingAnimation();
  menuitems.frame(frame_time);
  if (moving and not menuanim->isPlayingAnimation())
    anim_move.apply(menuitems);
}

void StateMenu::Draw()
{
  mApp.mWindow.draw(bg);
  mApp.mWindow.draw(arrow_left);
  mApp.mWindow.draw(arrow_right);
  menuitems.draw(mApp.mWindow, sf::RenderStates::Default);
  fader.draw(mApp.mWindow, sf::RenderStates::Default);
}

void StateMenu::HandleCleanup()
{
}

StateMenu::~StateMenu()
{
  delete anim_focus;
  delete anim_unfocus;
}

} /* moovie */

