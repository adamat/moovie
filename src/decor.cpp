/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "decor.hpp"

Decor::Decor(const char* path, unsigned fw, unsigned fh, unsigned fxc, unsigned
    fyc, unsigned short x, unsigned short y, unsigned char anim_type, bool
    slow_, int layer)
{
  rm->set_sprite(sprite, path, fw, fh, fxc, fyc);
  sprite->Moveto(x, y);
  frames = fxc;
  slow = slow_;
  flags = anim_type;
  delay = (flags & 0x3F) * 2;
  flags &= 0xC0; // strip the delay info
  layer_ = layer;
}

Decor::~Decor()
{
}

void Decor::make_view(const char* name, unsigned row, unsigned frames, unsigned
    char flags_)
{
  (*sprite)[name].SetView(row, frames, 0);
  views[name] = flags_;
}


void Decor::set_view(const char* name)
{
  (*sprite)[name];
  if(views.count(name)) flags = views[name];
  while(!sprite->Next()); // Return to first frame
  frame = 1;
  backwards = false;
}

void Decor::draw()
{
  if(autodraw_ == false) return;

  Drawable::draw();

  // static image
  if(deadframe) deadframe = false;

  else if(delayer == delay){
    if(frame < frames){
      if(backwards){
        if(frame == 1){
          if(!(flags & ONCE)){
            frame++;
            sprite->Next();
            backwards = false;
          }
        }
        else{
          frame--;
          sprite->Next(frames - 1);
          if(frame == 1) delayer = 0; //reset delay
        }
      }
      else{
        frame++;
        sprite->Next();
        if(frame == frames and !(flags & RETURN))
          delayer = 0; //reset delay
      }
    }
    else if(frame == frames){
      if(flags & RETURN){
        frame--;
        sprite->Next(frames - 1);
        backwards = true;
      }
      else if(!(flags & ONCE)){
        sprite->Next();
        frame = 1;
      }
    }

    if(slow) deadframe = true;
  }
  else delayer++;
}

void Decor::autodraw(bool ad)
{
  autodraw_ = ad;
}

int Decor::get_layer() const
{
  return layer_;
}
