/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Program state representing a level
 */

#ifndef STATE_LEVEL_H_OPLLQG40
#define STATE_LEVEL_H_OPLLQG40

#include <set>
#include <list>
#include <fstream>
#include <GQE/Core/interfaces/IState.hpp>
#include <GQE/Core/interfaces/TAssetHandler.hpp>
#include "constants.hpp"
#include "node.hpp"
#include "limiter.hpp"
#include "moovie.hpp"
#include "fader.hpp"

namespace moovie {

// Forward declarations
class Receiver;
class Limiter;
class Solid;
class Sensor;

class StateLevel : public GQE::IState
{
  private:
    // The scene grid
    Collidable* scene [NODES_X][NODES_Y];
    Node actors; // The root of almost all entities
    std::vector<Sensor*> sensors;

    // Level loading stuff
    std::ifstream file;
    unsigned level_number;
    std::string leveldef;
    void load_level() throw(std::exception);

    // TODO: transmitter-receiver "subsystem"
    std::map <unsigned char, std::set<Receiver*>* > receivers;

    // Game flags
    bool done_ = 0;
    bool won_ = false;
    bool lost_ = false;

    // Fader
    Fader fader;

    // Game quitting
    sf::Clock clock;
    const sf::Time time_to_quit = sf::seconds(1.5f);

    void remove(); //remove actors to be removed
    std::set <INode*> to_be_removed;

    sf::Texture background;
    sf::Sprite *bottom_bar;
    sf::Sprite *ball;
    unsigned char number_of_balls = 0;
    void redraw_balls();
    Solid *solid; // Class representing all unmovable "wall"  nodes

  protected:
    void HandleCleanup();

  public:
    StateLevel(GQE::IApp & parent, std::string level_file, unsigned level)
      throw(std::exception);
    virtual ~StateLevel();

    void DoInit() throw(std::exception);
    void ReInit() throw(std::exception) override;
    void HandleEvents(sf::Event theEvent);
    void UpdateFixed();
    void UpdateVariable(float frame_time);
    void Draw();

    // Do something with this:
    void remove_actor(INode* actor);

    /* TODO: make a transmitter-receiver "subsystem" */
    void add_receiver(Receiver* rec, unsigned char group);
    void activate_group(unsigned char group);

    void set_node(unsigned char x, unsigned char y, Collidable* node);
    Collidable* get_node(unsigned char x, unsigned char y);

    void quit(); //end the game
    void win();
    bool won() const;
    void lose();
    void update_balls(unsigned char number);

    // Useful "shortcuts"
    Collidable* air; //For moving via set_node()
    Collidable* limited; //Placeholder that may be in the scene grid
    Limiter* limiter; //When it's necessary to limit speed
    Moovie* moovie;
};

}

#endif /* end of include guard: STATE_LEVEL_H_OPLLQG40 */

