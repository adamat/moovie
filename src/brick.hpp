/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BRICK_H_668Y0CHJ
#define BRICK_H_668Y0CHJ

#include <SFML/Audio.hpp>
#include <GQE/Core/assets/SoundAsset.hpp>
#include "movable.hpp"

namespace moovie {

class Brick
: public Movable
{
  private:
    bool second_push[4] = {0,0,0,0}; // for every direction
    float fall_speed = 360.0f;
    bool will_fall = false;
    bool was_falling = false;

  protected:
    GQE::SoundAsset impact_sound_buffer;
    sf::Sound impact_sound;
    // Stuff for Glass to have life easier
    Brick (StateLevel & parent, unsigned char x, unsigned char y);
    virtual void press_side();
    virtual void press_top();

  public:
    Brick (StateLevel & parent, unsigned char x, unsigned char y, unsigned char
        asset);
    virtual ~Brick ();

    void collide_moovie(Collision & col, unsigned char side);
    void collide_brick(Collision & col, unsigned char side);
    void collide_machine(Collision & col, unsigned char side);
    void push_platform(Collision & col, unsigned char side);
    unsigned char get_type();

    void on_tick();
    void on_frame(float);
};

/*
 * Target
 * = the brick to be got at the base
 */

class Target
: public Brick
{
  public:
    Target (StateLevel & parent, unsigned char x, unsigned char y);
    virtual ~Target ();
};

}

#endif /* end of include guard: BRICK_H_668Y0CHJ */
