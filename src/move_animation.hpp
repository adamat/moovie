/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2016 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MOVE_ANIMATION_H_IA0H6AIF
#define MOVE_ANIMATION_H_IA0H6AIF

#include <SFML/System.hpp>

class MoveAnimation{
  private:
    sf::Vector2f start;
    sf::Vector2f translation;
  public:
    MoveAnimation() {}
    MoveAnimation(sf::Vector2f start, sf::Vector2f translation):
      start(start),
      translation(translation) {}
    virtual ~MoveAnimation() {}
    void set_start(sf::Vector2f newstart) {
      start = newstart;
    }
    void set_translation(sf::Vector2f newtrans) {
      translation = newtrans;
    }
    template <class Animated>
      void apply(Animated& animated) {
        start += translation;
        animated.setPosition(start);
    }
    template <class Animated>
      void operator() (Animated& animated, float progress) const {
        animated.setPosition(start + progress * translation);
    }
};

#endif /* end of include guard: MOVE_ANIMATION_H_IA0H6AIF */
