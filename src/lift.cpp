/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lift.hpp"
#include "state_level.hpp"
#include "constants.hpp"

namespace moovie {

LiftBase::LiftBase(StateLevel & parent, unsigned char x, unsigned char y,
    unsigned char to, bool vertical, sf::Time timeout) :
  Machine(parent, timeout),
  vert(vertical)
{
  init_xy(x, y, 1, 0);
  from_ = vert?y:x;
  to_ = to;

  dir = vert ? UP : LEFT;
  if(from_ < to_){
    dir = compl dir;
    dir &= 3;
  }
}

void LiftBase::activate(){
  will_start = true;
}

void LiftBase::start()
{
  if(!moving){
    set_direction(dir);
    moving = attempt_move(speed);
    if(!moving and (vert?tly:tlx) != from_){//if there is something in the way
      dir = compl dir;
      dir &= 3;

      // Swap from_ and to_
      from_ ^= to_;
      to_ ^= from_;
      from_ ^= to_;

      // Restart the timer
      timer.reset();
    }
    else if(!moving){
      timer.reset();
    }
    else{
      start_sound.play();
    }
    if(moving and dir == DOWN)
      limit_speed(speed);
  }
  will_start = false;
}

void LiftBase::on_frame(float frame_time)
{
  bool was_moving = moving;
  moving = not moving_done();

  if(was_moving and moving == false and (vert?tly:tlx) != to_){
    moving = attempt_move(speed);
    if(moving and dir == DOWN)
      limit_speed(speed);
  }

  if(was_moving and moving == false){ //just stopped
    dir = compl dir;
    dir &= 3;

    // Swap from_ and to_
    from_ ^= to_;
    to_ ^= from_;
    from_ ^= to_;

    // Restart the timer
    timer.reset();

    stop_sound.play();
  }

  if(will_start) start();

  Machine::on_frame(frame_time);
}

LiftBase::~LiftBase()
{
}

/*
 * Vertical lift
 */


std::function<void(sf::Sprite&, float)> Lift::rotate_anim;

void Lift::setup()
{
  rotate_anim = ([](sf::Sprite & animated, float progress){
      animated.setRotation(180 * progress); });
}

Lift::Lift(StateLevel & parent, unsigned char x, unsigned char y,
    unsigned char to_y, sf::Time timeout, unsigned char asset) :
  LiftBase(parent, x, y, to_y, true, timeout)
{
  auto sprite = mApp.make_sprite();
  switch(asset){
    default:
    case 0:
      sprite->setTextureRect({769, 452, 18, 18});
      break;
    case 1:
      sprite->setTextureRect({769, 472, 18, 18});
      break;
  }
  sprite->setOrigin(8.5, 8.5);
  sprite->setPosition(24, 13.5);
  push_back(sprite);

  sprite = mApp.make_sprite();
  switch(asset){
    default:
    case 0:
      sprite->setTextureRect({144, 288, 48, 24});
      break;
    case 1:
      sprite->setTextureRect({144, 312, 48, 24});
      break;
  }
  push_back(sprite);

  animator.addAnimation(0, thor::refAnimation(rotate_anim),
      sf::milliseconds(300));
  animator.playAnimation(0, true);

  start_sound_buffer.SetID("assets/sfx/lift_start.flac");
  start_sound.setBuffer(start_sound_buffer.GetAsset());
  stop_sound_buffer.SetID("assets/sfx/lift_stop.flac");
  stop_sound.setBuffer(stop_sound_buffer.GetAsset());

  speed = 80.0f;
}

Lift::~Lift()
{
}

void Lift::on_frame(float frame_time)
{
  animator.update(sf::seconds(frame_time));
  animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(0)));

  LiftBase::on_frame(frame_time);
}

void Lift::collide_brick(Collision & col, unsigned char side)
{
  col.deny();
}


void Lift::collide_moovie(Collision & col, unsigned char side)
{
  col.deny();
}

int Lift::get_layer() const
{
  return -10;
}


/*
 * Horisontal lift
 */

Platform::Platform(StateLevel & parent, unsigned char x, unsigned char y,
    unsigned char to_x, sf::Time timeout, unsigned char asset) :
  LiftBase(parent, x, y, to_x, false, timeout)
{
  auto sprite = mApp.make_sprite();
  switch(asset){
    default:
    case 0: // 33-35: Horizontal lift
      sprite->setTextureRect({336,288,48,24});
      break;
    case 1:
      sprite->setTextureRect({336,312,48,24});
      break;
  }

  push_back(sprite);

  speed = 120.0f;
}


bool Platform::attempt_step(unsigned char d)
{
  unsigned char tlx2 = tlx, tly2 = tly, brx2 = brx;
  if(Collidable::attempt_step(d)){
    if(mLevel.get_node(tlx2, tly2 - 1) == mLevel.get_node(brx2, tly2 - 1) and
        mLevel.get_node(tlx2, tly2 - 1) != mLevel.air){
      // Push the brick above
      Collision col; // won't really be taken into account
      col.set_max_velocity(get_velocity());
      mLevel.get_node(tlx2, tly2 - 1)->push_platform(col, (compl dir) & 3);
      // Delete the following if you want the brick to fall when something is
      // preventing its move (the platform would slip under the brick)
      if(!col.ok()){
        Collidable::attempt_step(compl d); //return underneath the brick
        return false;
      }
    }
    return true;
  }
  else return false;
}

Platform::~Platform()
{
}

} /* moovie */

