/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIMITER_H_OSLMUBHF
#define LIMITER_H_OSLMUBHF

#include <map>
#include "state_level.hpp"

/******* What the heck is this ******
 * Limiter is a part of scene graph. When a collidable moves at limited
 * velocity, it states that to the limiter. When another collidable wants to
 * move at a "limited node", it then must continue only some of the pixels at
 * its full speed, then continue at limited speed, so that the animations look
 * nice and smooth. The limiter takes the leaving collidable's speed into
 * account. Because getting the limits and calculating with them may cause
 * slowdowns, they are to be used only in the most relevant cases (like lifts).
 */

namespace moovie {

class Limiter
{
  private:
    StateLevel & mLevel;
    struct Limit{
      float v; // max. velocity, equals the leaving's velocity
      float cd; // countdown, delete the limit when 0;

    };
    std::map<unsigned short, Limit> limits;
    unsigned short make_key(unsigned char x, unsigned char y, unsigned char d);
    void delete_key(unsigned short k);

  public:
    Limiter(StateLevel & parent);
    virtual ~Limiter();
    void frame(float frame_time); //update countdowns

    unsigned char get_limit_speed(unsigned char x, unsigned char y, unsigned
        char direction);
    unsigned char get_remaining_pixels(unsigned char x, unsigned char y,
        unsigned char direction);

    void set_limit(unsigned char x, unsigned char y, unsigned char direction,
        unsigned char limit);
};

}

#endif /* end of include guard: LIMITER_H_OSLMUBHF */
