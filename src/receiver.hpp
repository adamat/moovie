/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/******************
  Moovie - Receiver class
 ******************/

#ifndef RECEIVER_H_YP46P5HQ
#define RECEIVER_H_YP46P5HQ

namespace moovie {

class Receiver{
  protected:
    Receiver();
    virtual ~Receiver();

  public:
    virtual void activate() = 0;
};

}

#endif /* end of include guard: RECEIVER_H_YP46P5HQ */

