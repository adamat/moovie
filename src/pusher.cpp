/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pusher.hpp"
#include "state_level.hpp"
#include "constants.hpp"

namespace moovie {

Pusher::Pusher(StateLevel & parent, unsigned char x, unsigned char y,
    unsigned char tox, unsigned char asset) :
  Machine(parent, sf::milliseconds(0))
{
  auto s = mApp.make_sprite();
  to_left = (tox > x) ? false : true;
  sf::FloatRect bodysrc;
  sf::IntRect backbodysrc;

  unsigned asset_x = (asset) ? 384 : 288;
  if(not to_left){
    bodysrc = {asset_x + 48, 336, 24, 48};
    backbodysrc = {asset_x + 72, 336, 24, 48};
  }
  else{
    bodysrc = {asset_x + 24, 336, 24, 48};
    backbodysrc = {asset_x, 336, 24, 48};
  }
  s->setTextureRect(backbodysrc);
  body = new RepeatedQuad(&mApp.get_texture_atlas(), bodysrc, !to_left);
  if(to_left)
    body->move(24, 0);
  push_back(s);
  push_back(body);

  length = (to_left)? x - tox : tox - x;

  init_xy(x, y, 0, 1);
}


void Pusher::activate()
{
  if(not moving and not will_back){
    set_direction(to_left ? LEFT : RIGHT);
    will_move = true;
  }
}

void Pusher::on_tick()
{
  if(will_move){
    will_move = false;
    moving = attempt_move(velocity);
    if(moving){
      length_counter++;

      unsigned char px = tlx + (to_left ? 1 : -1);
      mLevel.set_node(px, tly, this);
      mLevel.set_node(px, bry, this);
    }
  }
  else if(not back and will_back){
    if(delay < 20) delay++;
    else{
      back = true;
      will_back = false;
      set_direction(to_left ? RIGHT : LEFT);
      pending_collision = true;
      moving = attempt_move(velocity);
      pending_collision = false;
      length_counter--;
      delay = 0;
    }
  }
}


void Pusher::on_frame(float frame_time)
{
  Machine::on_frame(frame_time);

  if(moving and not back){
    float delta = velocity * frame_time;
    bool quad_done = body->change_size(delta);
    if(quad_done){
      if(length_counter < length and attempt_move(velocity)){
        length_counter++;

        unsigned char px = tlx + (to_left ? 1 : -1);
        mLevel.set_node(px, tly, this);
        mLevel.set_node(px, bry, this);
      }
      else{
        moving = false;
        will_back = true;
      }
    }
  }
  else if(moving and back){
    float delta = velocity * frame_time;
    bool quad_done = body->change_size(-delta);
    if(quad_done){
      if(length_counter > 0){
      pending_collision = true;
      attempt_move(velocity);
      pending_collision = false;
      length_counter--;
      }
      else{
        back = false;
        moving = false;
      }
    }
  }
}

Pusher::~Pusher()
{
}

void Pusher::collide_machine(Collision & col, unsigned char side)
{
  if(pending_collision) col.approve();
  else col.deny();
}

} /* moovie */

