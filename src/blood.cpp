/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "blood.hpp"
#include "state_level.hpp"
#include "solid.hpp"
#include "constants.hpp"

namespace moovie {

thor::FrameAnimation Blood::frame_anim;

void Blood::setup()
{
  frame_anim.addFrame(8.0f, {672, 496, 15, 14});
  for(int i = 1; i < 6; i++)
    frame_anim.addFrame(1.0f, {15 * i + 672, 496, 15, 14});
}

Blood::Blood(StateLevel & parent, unsigned x_, unsigned y_,
    float vx_, float vy_) :
  Node(parent),
  x(x_),
  y(y_),
  vx(vx_),
  vy(vy_)
{
  auto sprite = mApp.make_sprite();
  sprite->setTextureRect({672, 496, 15, 14});
  sprite->setOrigin(6, 4);
  push_back(sprite);

  setPosition(x, y);

  animator.addAnimation(1, thor::refAnimation(frame_anim), sf::milliseconds(600));
}

void Blood::on_frame(float frame_time)
{
  float & t = frame_time;
  // Moving the blood
  if(!x_stopped or !y_stopped){
    tx = x + vx * t;
    ty = y + vy * t;

    // Try and bounce
    if(mLevel.get_node(tx / NODE_SIZE, ty / NODE_SIZE) != mLevel.air and
        mLevel.get_node(tx / NODE_SIZE, ty / NODE_SIZE)->stops_blood()){
      // Bounce against vertical wall
      if(mLevel.get_node(tx / NODE_SIZE, y / NODE_SIZE) != mLevel.air and
          mLevel.get_node(tx / NODE_SIZE, y / NODE_SIZE)->stops_blood()){
        vx *= -1;
        vx *= x_bounce;
      }
      // Bounce against horizontal wall
      if(mLevel.get_node(x / NODE_SIZE, ty / NODE_SIZE) != mLevel.air and
          mLevel.get_node(x / NODE_SIZE, ty / NODE_SIZE)->stops_blood()){
        vy *= y_bounce;
        if(vy < 0 or vy >= gravity * t){
          vy *= -1;
        }
        else
        {
          y_stopped = true;
          vy = 0;
        }
      }
    }

    // Apply
    x = x + vx * t;
    y = y + vy * t;

    setPosition(x, y);

    // Check, whether to reactivate gravity
    if(y_stopped and !x_stopped and
        (mLevel.get_node(x / NODE_SIZE, (y / NODE_SIZE) + 1) == mLevel.air or
        (not mLevel.get_node(x / NODE_SIZE, (y / NODE_SIZE) + 1)->stops_blood()
         ))){
      y_stopped = false;
    }

    // Change vectors
    if(!y_stopped)
      vy += t * gravity;
    else if((vx<0?-vx:vx) < ground_friction * t)
      vx = 0;
    else
      vx += ((vx<0)?ground_friction:-ground_friction) * t;

    if((vx<0?-vx:vx) < friction * t){
      vx = 0;
      x_stopped = true;
    }
    else
      vx += ((vx<0)?friction:-friction) * t;

    if((vy<0?-vy:vy) < friction * t){
      vy = 0;
      y_stopped = true;
    }
    else
      vy += ((vy<0)?friction:-friction) * t;

    if(y_stopped and x_stopped)
      animator.playAnimation(1);
  }
  // Blood has stopped
  else{
    animator.update(sf::seconds(frame_time));
    animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(0)));
  }
}

Blood::~Blood()
{
}

} /* moovie */

