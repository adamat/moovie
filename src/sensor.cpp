/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sensor.hpp"
#include "state_level.hpp"

namespace moovie {

Sensor::Sensor(StateLevel & parent, unsigned char x, unsigned char y,
    unsigned char types, unsigned char group) :
  Emitter(parent, group),
  sensorees(types),
  x_(x),
  y_(y),
  last(level.air)
{
  ;
}

void Sensor::on_tick()
{
  // Check for changes
  Collidable *tmp = level.get_node(x_, y_);
  if(tmp != last){
    last = tmp;
    if(last != level.air)
      last_type = last->get_type();
    else
      last_type = TYPE_NONE;
  }

  //Emit
  if(sensorees & last_type){ // the object is "seen"
    emit();
  }
}

Sensor::~Sensor()
{
}

} /* moovie */

