/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "teleport.hpp"
#include "state_level.hpp"
#include "constants.hpp"

namespace moovie {

thor::FrameAnimation Teleport::idle_m_anim[2];
thor::FrameAnimation Teleport::busy_m_anim[2];
thor::FrameAnimation Teleport::idle_f_anim;
thor::FrameAnimation Teleport::prep_f_anim;
thor::FrameAnimation Teleport::flash_f_anim;
thor::FrameAnimation Teleport::after_f_anim;

void Teleport::setup()
{
  for(int i = 0; i < 6; i++){
    idle_m_anim[0].addFrame(1.0f, {i * 48 + 384, 288, 48, 24});
    idle_m_anim[1].addFrame(1.0f, {i * 48 + 384, 312, 48, 24});
  }
  for(int i = 4; i > 0; i--){
    idle_m_anim[0].addFrame(1.0f, {i * 48 + 384, 288, 48, 24});
    idle_m_anim[1].addFrame(1.0f, {i * 48 + 384, 312, 48, 24});
  }

  busy_m_anim[0].addFrame(1.0f, {672, 288, 48, 24});
  busy_m_anim[1].addFrame(1.0f, {672, 312, 48, 24});

  idle_f_anim.addFrame(1.0, {0, 511, 1, 1});

  for(int i = 0; i < 9; i++){
    prep_f_anim.addFrame(1.0f, {96, 384, 48, 48});
    prep_f_anim.addFrame(1.0f, {48, 384, 48, 48});
    prep_f_anim.addFrame(1.0f, { 0, 384, 48, 48});
    prep_f_anim.addFrame(1.0f, {48, 384, 48, 48});
  }
  prep_f_anim.addFrame(1.0f, {0,  384, 48, 48});

  for(int i = 0; i < 6; i++)
    flash_f_anim.addFrame(1.0, {i * 48 + 144, 384, 48, 48});

  for(int i = 0; i < 6; i++)
    after_f_anim.addFrame(1.0+i/2.0, {i * 48 + 432, 384, 48, 48});
}

Teleport::Teleport(StateLevel & parent, unsigned char x1, unsigned char y1,
    unsigned char x2, unsigned char y2, sf::Time timeout, unsigned char asset):
  Node(parent),
  x{x1, x2},
  y{y1, y2},
  timer(this, timeout)
{
  m_animator.addAnimation(0, thor::refAnimation(idle_m_anim[asset]),
      sf::seconds(0.75f));
  m_animator.addAnimation(1, thor::refAnimation(busy_m_anim[asset]),
      sf::milliseconds(1));
  f_animator.addAnimation(0, thor::refAnimation(idle_f_anim),
      sf::milliseconds(1));
  f_animator.addAnimation(1, thor::refAnimation(prep_f_anim),
      sf::seconds(0.8f));
  f_animator.addAnimation(2, thor::refAnimation(flash_f_anim),
      sf::seconds(0.20f));
  f_animator.addAnimation(3, thor::refAnimation(after_f_anim),
      sf::seconds(0.6f));

  auto sprite = mApp.make_sprite();
  sprite->setPosition(x1 * NODE_SIZE, y1 * NODE_SIZE);
  push_back(sprite);
  sprite = mApp.make_sprite();
  sprite->setPosition(x2 * NODE_SIZE, y2 * NODE_SIZE);
  push_back(sprite);

  m_animator.playAnimation(0, true);
  f_animator.playAnimation(0, true);

  aftereffect_sound_buffer.SetID("assets/sfx/teleport.flac");
  aftereffect_sound.setBuffer(aftereffect_sound_buffer.GetAsset());
}


void Teleport::add_sprite(unsigned char port, unsigned char asset)
{
  for(int i = 0; i <= 1; i++){
    if(port & (i + 1) and m[i] == nullptr){
      auto t = mApp.make_sprite();
      switch(asset){
        default:
        case 0:
          t->setTextureRect({720,288,48,24});
          break;
        case 1:
          t->setTextureRect({720,312,48,24});
          break;
      }

      t->setPosition(x[i] * NODE_SIZE, (y[i] - 1) * NODE_SIZE);
      push_back(t);
      m[i] = mApp.make_sprite();
      m[i]->setPosition(x[i] * NODE_SIZE, (y[i] + 2) * NODE_SIZE);
      push_back(static_cast<DrawableNode<sf::Sprite>*>(m[i]));
    }
  }
}


void Teleport::activate()
{
  if(!busy){
    busy = 1;
  }
}

void Teleport::on_tick()
{
  timer.tick();

  if(busy == 1){
    m_animator.playAnimation(1, true);
    f_animator.playAnimation(1);
    busy = 2;
  }
  else if(busy == 2 and not f_animator.isPlayingAnimation()){
    f_animator.playAnimation(2);
    busy = 3;
  }
  else if(busy == 3 and not f_animator.isPlayingAnimation()){
    busy = 6; // just to ensure passing another condition
    // Check both ports
    if(mLevel.get_node(x[0], y[0]) == mLevel.get_node(x[0] + 1, y[0]) and
        mLevel.get_node(x[0] + 1, y[0]) == mLevel.get_node(x[0], y[0] + 1) and
        mLevel.get_node(x[0], y[0] + 1) == mLevel.get_node(x[0] + 1, y[0] + 1)
        and mLevel.get_node(x[1], y[1]) == mLevel.get_node(x[1] + 1, y[1]) and
        mLevel.get_node(x[1] + 1, y[1]) == mLevel.get_node(x[1], y[1] + 1) and
        mLevel.get_node(x[1], y[1] + 1) == mLevel.get_node(x[1] + 1, y[1] + 1)
        and (mLevel.get_node(x[0], y[0]) == mLevel.air xor
         mLevel.get_node(x[1], y[1]) == mLevel.air)){
      //Porting itself
      Collidable *p1 = mLevel.get_node(x[0], y[0]);
      if(p1 == mLevel.air){
        mLevel.get_node(x[1], y[1])->teleport(x[0], y[0]);
        if(mLevel.get_node(x[1], y[1]) == mLevel.air){ // Teleporting succesful
          busy = 5;
          f_animator.playAnimation(0, true);
          f_animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(0)));
          f_animator.playAnimation(3);
          aftereffect_sound.play();
        }
      }
      else{
        p1->teleport(x[1], y[1]);
        if(mLevel.get_node(x[0], y[0]) == mLevel.air){ // Teleporting succesful
          busy = 4;
          f_animator.playAnimation(0, true);
          f_animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(1)));
          f_animator.playAnimation(3);
          aftereffect_sound.play();
        }
      }
    }
  }
  // Here is no 'else', because when nothing is teleported = no after-effect
  if(busy >= 4 and not f_animator.isPlayingAnimation()){
    busy = 0;
    timer.reset();
    m_animator.playAnimation(0, true);
    f_animator.playAnimation(0, true);
  }
}

void Teleport::on_frame(float frame_time)
{
  f_animator.update(sf::seconds(frame_time));
  if(busy >=4)
    f_animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(busy%4)));
  else{
    f_animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(0)));
    f_animator.animate(*static_cast<DrawableNode<sf::Sprite>*>(at(1)));
  }

  m_animator.update(sf::seconds(frame_time));
  for(auto &i : m){
    if(i != nullptr)
      m_animator.animate(*i);
  }
}

Teleport::~Teleport()
{
}

} /* moovie */
