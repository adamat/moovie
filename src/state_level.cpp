/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// NOTE: the level-loading part of StateLevel is in loader.cpp
#include <iostream>
#include <system_error>
#include <ctime>
#include <libintl.h>
#include <algorithm>
#include <limits>

#include "state_level.hpp"
#include "receiver.hpp"
#include "collidable.hpp"
#include "solid.hpp"
#include "sensor.hpp"
#include "ball.hpp"

#define _(STRING) gettext(STRING)

namespace moovie {

StateLevel::StateLevel(GQE::IApp & parent,
    std::string level_file, unsigned level) throw(std::exception) :
  GQE::IState("moovie", parent),
  file(level_file.c_str()),
  level_number(level),
  actors(*this),
  bottom_bar(actors.mApp.make_sprite()),
  ball(actors.mApp.make_sprite()),
  fader(sf::milliseconds(400)),
  solid(nullptr)
{
  if(!file)
    throw std::system_error(errno, std::system_category());
}

void StateLevel::DoInit() throw(std::exception)
{
  IState::DoInit();

  limiter = new Limiter(*this);
  limited = new Collidable(*this);
  air = nullptr;

  ball->setTextureRect({666,496,6,6});
  ball->setOrigin(3, 3);
  bottom_bar->setTextureRect({0,432,768,64});
  bottom_bar->setPosition(0, NODES_Y * NODE_SIZE);

  // Find the level in the file
  char c;
  unsigned n;
  do{
    file >> c;
    if(c == '%') file >> n;
    else
      file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  } while(n != level_number and !file.eof());

  if(file.eof()){
    mApp.mStateManager.RemoveActiveState();
    throw std::runtime_error(_("Level not found"));
  }

  std::getline(file, leveldef, '%');

  ReInit();
}

void StateLevel::ReInit() throw(std::exception)
{
  // Delete entire scene
  for(int y = 0; y < NODES_Y; y++){
    for(int x = 0; x < NODES_X; x++){
      scene[x][y] = air;
    }
  }

  // Clear the scenegraph
  // Delete in case ReInit is called for actual reinitialisation
  for(auto i : actors){
    delete i;
  }
  for(auto i : sensors){
    delete i;
  }
  for(auto i : receivers){
    delete i.second;
  }
  actors.clear();
  receivers.clear();

  if(solid)
    delete solid;
  solid = new Solid(*this);

  // Edge walls:
  for(int i = 0; i < NODES_X; i++){
    scene[i][0] = solid;
    scene[i][21] = solid;
  }
  for(int i = 1; i < NODES_Y; i++){
    scene[0][i] = solid;
    scene[31][i] = solid;
  }

  //Create the level
  try{
    load_level();
  } catch (std::exception &e){
    mApp.mStateManager.RemoveActiveState();
    throw;
  }

  fader.fade_in();
}

void StateLevel::quit()
{
  done_ = true;
}

void StateLevel::win()
{
  if(!lost_){
    won_ = true;
    moovie->disable_moving();
    remove_actor(moovie);
    clock.restart();
  }
}

bool StateLevel::won() const
{
  return won_;
}

void StateLevel::lose()
{
  lost_ = true;
  clock.restart();
}

void StateLevel::redraw_balls()
{
  mApp.mWindow.draw(*bottom_bar);
  for(int i = 1; i <= number_of_balls and i <= 16; i++){
    ball->setPosition(280 + i * 12, NODES_Y * NODE_SIZE + 33);
    mApp.mWindow.draw(*ball);
  }
}

void StateLevel::update_balls(unsigned char number)
{
  number_of_balls = number;
}

void StateLevel::remove_actor(INode* actor)
{
  to_be_removed.insert(actor);
}

void StateLevel::remove(){
  for(auto i : to_be_removed){
    delete i;
    auto it = std::find(actors.begin(), actors.end(), i);
    if(it != actors.end())
      actors.erase(it);
  }
  to_be_removed.clear();
}

void StateLevel::add_receiver(Receiver* rec, unsigned char group)
{
  //create the set if needed
  if(!receivers.count(group)){
    receivers[group] = new std::set<Receiver*>;
  }

  receivers[group]->insert(rec);
}

void StateLevel::activate_group(unsigned char group)
{
  if(!receivers.count(group)) return;
  for(std::set<Receiver*>::iterator r = receivers[group]->begin();
      r != receivers[group]->end(); r++){
    (*r)->activate();
  }
}

void StateLevel::set_node(unsigned char x, unsigned char y, Collidable* node)
{
  if(x < NODES_X and y < NODES_Y)
    scene[x][y] = node;
}

Collidable* StateLevel::get_node(unsigned char x, unsigned char y)
{
  if(x < NODES_X and y < NODES_Y)
    return scene[x][y];
  else return nullptr;
}

void StateLevel::HandleEvents(sf::Event theEvent)
{
  auto & ev = theEvent;
  switch(ev.type){
    case sf::Event::KeyPressed:
      if(ev.key.code == sf::Keyboard::Q and not (won_ or lost_))
        quit();
      if(ev.key.code == sf::Keyboard::Space){
        auto b = moovie->shoot();
        if(b != nullptr)
          actors.push_back(b);
      }
      break;
    default:
      break;
  }
}

void StateLevel::UpdateFixed()
{
  if(done_ or ((won_ or lost_) and clock.getElapsedTime() >= time_to_quit)){
    fader.fade_out([&](){
        mApp.mStateManager.RemoveActiveState();
        });
  }

  actors.tick();

  for(auto i : sensors){
    i->on_tick();
  }

  remove();
}

void StateLevel::UpdateVariable(float frame_time)
{
  actors.frame(frame_time);
  limiter->frame(frame_time);
  fader.frame(frame_time);
}

void StateLevel::Draw()
{

  redraw_balls();
  actors.draw(mApp.mWindow, sf::RenderStates::Default);
  fader.draw(mApp.mWindow, sf::RenderStates::Default);

  // TODO: on-screen debugging info
}

StateLevel::~StateLevel()
{
  delete ball;
  delete bottom_bar;
}

void StateLevel::HandleCleanup()
{
  delete limiter;
  delete limited;
  delete solid;
  limiter = nullptr;
  limited = nullptr;
  solid = nullptr;
  for(auto i : sensors){
    delete i;
  }
  sensors.clear();
}

}

