/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <memory>
#include <exception>
#include <unistd.h>
#include <libgen.h>
#include <SFML/Config.hpp>
#include "GQE/Core.hpp"
#include "app.hpp"

#define _(STRING) gettext(STRING)

/*
#ifdef SFML_SYSTEM_LINUX
#include <X11/Xlib.h>
#endif
*/

int main(int argc, char **argv)
{
  //Locale stuff
  setlocale(LC_ALL, "");
  bindtextdomain("moovie", "locale");
  textdomain("moovie");

/*
#ifdef SFML_SYSTEM_LINUX
  // needed for multithreading to work
  XInitThreads();
#endif
*/

// First of all, change directory to the executable's one
  if(chdir(dirname(argv[0]))){
    perror(_("Cannot change directory"));
    return errno;
  }

  GQE::FileLogger logger("moovie.log", true);

  int exit_code = GQE::StatusNoError;
  moovie::App *main_app;

  try{
    main_app = new moovie::App();
  }
  catch (std::exception & e){
    std::cerr << _("Couldn't initialise the application: ") << e.what() << "\n";
    return 1;
  }

  exit_code = main_app->Run();

  return exit_code;
}

