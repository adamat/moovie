/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/******************
  Moovie - Creature class
 ******************/

#ifndef CREATURE_H_EI6T655T
#define CREATURE_H_EI6T655T

#include <Thor/Animation.hpp>
#include <GQE/Core/assets/SoundAsset.hpp>
#include "movable.hpp"

namespace moovie {

class Creature : public Movable
{
  protected:
    bool dead = false;
    GQE::SoundAsset bloodsplash_sound_buffer;
    sf::Sound bloodsplash_sound;

    static thor::FrameAnimation death_anim;
    thor::Animator <sf::Sprite, const char*> animator;

    Creature (StateLevel & parent, unsigned char x, unsigned char y);
    virtual ~Creature ();

  public:
    static void setup();
    void die();

    void collide_brick(Collision & col, unsigned char side);
};

}

#endif /* end of include guard: CREATURE_H_EI6T655T */

