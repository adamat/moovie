/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "brick.hpp"
#include "constants.hpp"

namespace moovie {

Brick::Brick(StateLevel & parent, unsigned char x, unsigned char y) :
  Movable(parent)
{
  init_xy(x, y, 1, 1);

  //Assign collision function
  collide_func = &Collidable::collide_brick;
}

Brick::Brick(StateLevel & parent, unsigned char x, unsigned char y, unsigned
    char asset):
  Brick(parent, x, y)
{
  auto sp = mApp.make_sprite();
  switch(asset){
    case 0:
      sp->setTextureRect({336,192,48,48});
      break;
    case 1:
      sp->setTextureRect({384,192,48,48});
      break;
    case 2:
      sp->setTextureRect({432,192,48,48});
      break;
    case 3:
      sp->setTextureRect({480,192,48,48});
      break;
    case 10:
      sp->setTextureRect({528,192,48,48});
      break;
    case 11:
      sp->setTextureRect({576,192,48,48});
      break;
    case 12:
      sp->setTextureRect({624,192,48,48});
      break;
    case 13:
      sp->setTextureRect({672,192,48,48});
      break;
    case 20: // Target brick
      sp->setTextureRect({288,192,48,48});
      break;
    default:
      sp->setTextureRect({720,192,48,48});
      break;
  }

  push_back(sp);

  impact_sound_buffer.SetID("assets/sfx/brick_fall.flac");
  impact_sound.setBuffer(impact_sound_buffer.GetAsset());
}

Brick::~Brick()
{
  ;
}

void Brick::press_side()
{
  ;
}

void Brick::press_top()
{
  ;
}

void Brick::collide_moovie(Collision & col, unsigned char side)
{
  side &= 3;
  if(not moving_done()){
    col.deny();
    return;
  }

  switch(side){
    case UP:
      set_direction(DOWN);
      if(attempt_move(fall_speed)){
        col.approve();
        col.set_max_velocity(get_velocity());
      }
      else{
        col.deny();
        press_top();
      }
      break;
    case DOWN:
      col.deny();
      break;
    case LEFT:
    case RIGHT:
      if(!second_push[side]){
        second_push[side] = true;
        col.deny();
      }
      else{
        set_direction((compl side) & 3);
        if(attempt_move(col.velocity())){
          col.absolute_approve();
        }
        else col.deny();
        press_side();
      }
  }
}

void Brick::collide_brick(Collision & col, unsigned char side)
{
  side &= 3;
  if(not moving_done()){
    col.deny();
    return;
  }

  if(side == UP){
    set_direction(DOWN);
    if(attempt_move(fall_speed)){
      col.approve();
      col.set_max_velocity(get_velocity());
    }
    else{
      col.deny();
    }
  }
  else col.deny();
}

void Brick::collide_machine(Collision & col, unsigned char side)
{
  side &= 3;
  if(not moving_done()){
    col.deny();
    return;
  }

  switch(side){
    case UP:
      col.deny();
      break;
    case DOWN:
      set_direction((compl side)&3);
      if(second_push[side]){
        if(attempt_move(col.velocity())){
          col.absolute_approve();
        }
        else col.deny();
      }
      else{
        second_push[side] = true;
        col.deny();
      }
      break;
    case LEFT:
    case RIGHT:
      if(!second_push[side]){
        second_push[side] = true;
        col.deny();
      }
      else{
        set_direction((compl side) & 3);
        if(attempt_move(col.velocity())){
          col.absolute_approve();
        }
        else col.deny();
        break;
        default: col.deny();
      }
  }
}

void Brick::push_platform(Collision & col, unsigned char side)
{
  side &= 3;
  if(not moving_done()){
    col.deny();
    return;
  }

  set_direction((compl side)&3);
  if(attempt_move(col.velocity())){
    col.approve();
  }
  else{
    col.deny();
  }
}

unsigned char Brick::get_type()
{
  return TYPE_BRICK;
}

void Brick::on_tick()
{
}

void Brick::on_frame(float frame_time)
{

  if(will_fall){ // Delayed falling
    if(moving_done()){

      set_direction(DOWN);
      auto old_speed = get_velocity();
      will_fall = !attempt_move(fall_speed);
      if(was_falling and will_fall and old_speed == fall_speed)
        impact_sound.play();
      was_falling = !will_fall;
    }
    else{
      will_fall = false;
    }
  }

  Movable::on_frame(frame_time);

  if(moving_done()){
    if(get_direction() == DOWN){
      will_fall = true; // delay falling
    }
    else{
      set_direction(DOWN);
      will_fall = !attempt_move(fall_speed);
      was_falling = !will_fall;
    }
  }

  second_push[RIGHT] = 0;
  second_push[LEFT] = 0;
  second_push[DOWN] = 0;
  second_push[UP] = 0;
}

/*
 * Target
 */

Target::Target(StateLevel & parent, unsigned char x, unsigned char y):
  Brick(parent, x, y, 20)
{
  collide_func = &Collidable::collide_target;
}

Target::~Target()
{
  ;
}

}

