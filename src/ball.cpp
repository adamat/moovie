/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ball.hpp"
#include "state_level.hpp"
#include "solid.hpp"
#include "constants.hpp"

namespace moovie {

Ball::Ball(StateLevel & parent, unsigned x_, unsigned y_, float vx_, float vy_):
  Node(parent),
  x(x_),
  y(y_),
  vx(vx_),
  vy(vy_)
{
  auto sprite = mApp.make_sprite();
  sprite->setTextureRect({666,496,6,6});
  sprite->setOrigin(3.0f, 3.0f);
  push_back(sprite);

  setPosition(x, y);
}

void Ball::on_tick()
{
  // Check for Moovie
  if(not remove and (harmless or y_stopped) and
      mLevel.get_node(x / NODE_SIZE, y / NODE_SIZE) != mLevel.air and
      mLevel.get_node(x / NODE_SIZE, y / NODE_SIZE)->get_type() == TYPE_MOOVIE){
    remove = mLevel.get_node(x / NODE_SIZE, y / NODE_SIZE)->collide_ball();
  }

  if(remove)
    mLevel.remove_actor(this);
}

void Ball::on_frame(float frame_time)
{
  float & t = frame_time;
  // Check, whether to reactivate gravity
  if(y_stopped and
      (mLevel.get_node(x / NODE_SIZE, (y / NODE_SIZE) + 1) == mLevel.air or
      (mLevel.get_node(x / NODE_SIZE, (y / NODE_SIZE) + 1) == mLevel.limited))){
    harmless = false;
    y_stopped = false;
  }

  if(!x_stopped or !y_stopped){
    tx = x + vx * t;
    ty = y + vy * t;

    // Try and bounce
    if(mLevel.get_node(tx / NODE_SIZE, ty / NODE_SIZE) != mLevel.air and
        mLevel.get_node(tx / NODE_SIZE, ty / NODE_SIZE) != mLevel.limited){
      // Bounce against vertical wall
      if(!harmless and
          mLevel.get_node(tx / NODE_SIZE, y / NODE_SIZE) != mLevel.air and
          mLevel.get_node(tx / NODE_SIZE, y / NODE_SIZE) != mLevel.limited){
        vx *= -1;
        vx *= x_bounce;
        if(not harmless and not remove)
          remove = mLevel.get_node(tx / NODE_SIZE, y /
              NODE_SIZE)->collide_ball();
      }
      // Bounce against horizontal wall
      if((harmless and
            mLevel.get_node(x / NODE_SIZE, ty / NODE_SIZE) != mLevel.air) or
          (!harmless and
           mLevel.get_node(x / NODE_SIZE, ty / NODE_SIZE) != mLevel.air and
           mLevel.get_node(x / NODE_SIZE, ty / NODE_SIZE) != mLevel.limited)){
        vy *= y_bounce;
        if(vy < 0 or vy >= (harmless ? 30.0f : 68.0f)){
          vy *= -1;
          if(not harmless and not remove)
            remove = mLevel.get_node(x / NODE_SIZE,
                ty / NODE_SIZE)->collide_ball();
        }
        else
        {
          y_stopped = true;
          vy = 0;
          // Align with the floor
          y = round(ty / NODE_SIZE) * NODE_SIZE - 1;
          static_cast<DrawableNode<sf::Sprite>*>(at(0))->setOrigin(3, 5);
        }
      }
    }

    if(vx > -1.3f and vx < 1.3f){
      x_stopped = true;
      vx = 0;
    }

    // Apply
    x = x + vx * t;
    y = y + vy * t;

    setPosition(x, y);

    // Change vectors
    if(!y_stopped)
      vy += gravity * t;
    else if((vx<0?-vx:vx) < ground_friction * t)
      vx = 0;
    else
      vx += ((vx<0)?ground_friction:-ground_friction) * t;

    if((vx<0?-vx:vx) < friction * t)
      vx = 0;
    else
      vx += ((vx<0)?friction:-friction) * t;

    if((vy<0?-vy:vy) < friction * t)
      vy = 0;
    else
      vy += ((vy<0)?friction:-friction) * t;
  }
  // Ball has stopped
  else if(mLevel.get_node(x / NODE_SIZE, y / NODE_SIZE) == mLevel.air){
    // Possibly start bouncing
    if(delay){
      delay -= t;
      if(delay <= 0){
        delay = 0;
        harmless = true;
        vy = -110.0f;
        y_stopped = false;
      }
    }
    else if(!delay){
      delay = 3 + (rand() % 7);
      delay /= 2;
    }
  }
}

void Ball::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  if(not remove)
    Node::draw(target, states);
}

Ball::~Ball()
{
}

} /* moovie */

