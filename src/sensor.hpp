/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SENSOR_H_5GI89XVP
#define SENSOR_H_5GI89XVP

#include "emitter.hpp"
#include "collidable.hpp"

namespace moovie {

enum { //Extension
  TYPE_CREATURE = 6 //All types of creatures
};

class Sensor :
  public Emitter
{
  private:
    unsigned char x_, y_;
    unsigned char sensorees; // types the sensor "sees"
    unsigned char last_type = TYPE_NONE;
    Collidable *last;

  public:
    Sensor (StateLevel & parent, unsigned char x, unsigned char y,
        unsigned char types, unsigned char group);
    virtual ~Sensor ();

    void on_tick();
};

} /* moovie */

#endif /* end of include guard: SENSOR_H_5GI89XVP */
