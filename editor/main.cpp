/* Moovie - the open source clone of logic game Boovie
 * Copyright (C) 2014 Adam Matoušek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <masdl/masdl.hpp>
#include <SDL/SDL_ttf.h>
#include <iostream>
#include <clocale>
#include <unistd.h>
#include <libintl.h>
#define _(STRING) gettext(STRING)

using namespace MAsdl;

Display *disp;
Surface *bg;     // Loaded background or solid color
Surface *cur;    // "Cursor" - highlighted node
Surface *marks;  // Already written nodes
Surface *stat;   // Status line

Color wall(60, 250, 30, 100);
Color brick(60, 60, 65, 200);
Color glass(0, 10, 200, 100);
Color button(30, 100, 160, 200);
Color lift(60, 60, 250, 100);
Color lift2(60, 60, 250, 70);
Color target(250, 230, 10, 200);
Color moovie(250, 170, 10, 200);
Color base(230, 200, 50, 200);
Color port(160, 200, 30, 200);
Color sensor(5, 200, 132, 160);
SDL_Color stat_fg = {160, 160, 160};
SDL_Color stat_bg = {24, 24, 24};
TTF_Font *font;

const unsigned char NX = 32, NY = 22, NS = 24;
bool done = false;
unsigned char hix = NX + 1, hiy;
bool nodes[NX][NY];
char ch = '.';
char shp = 0; //0 = . 1 = .. 2 = : 3 = ::
Color *co = &wall;
char lx = -1, ly = -1; //Locked axes
bool secport = 0; //placement of second teleport
bool hm = 0, ht = 0, hb = 0; //Moovie/target/base has been already placed

void status(const char *st){
  delete stat;
  stat = new Surface(TTF_RenderUTF8_Shaded(font, st, stat_fg, stat_bg));
  disp->FRect(Rect(0, NY * NS, NX * NS, 24), stat_bg.r, stat_bg.g,
      stat_bg.b);
}
void mark (unsigned char hix, unsigned char hiy, bool writeout = true){
  if(nodes[hix][hiy] == false or ch != '.'){
    marks->FRect(Rect(hix * NS, hiy * NS, shp&1 ? 2*NS : NS, shp&2 ? 2*NS :
          NS), *co);
    if(writeout == false) return;
    else if(lx >= 0){
      std::cout << " " << (unsigned)hiy << " 0 assets/lift.png 0" << std::endl;
      lx = -1;
      co = &lift;
      status(_("Vertical lift"));
    }
    else if(ly >= 0){
      ly = -1;
      co = &lift;
      if(ch == 'h'){
        std::cout <<" "<<(unsigned)hix<<" 0 assets/platform.png 0"<<std::endl;
        status(_("Horisontal platform"));
      }
      else if(ch == 'p'){
        std::cout <<" "<<(unsigned)hix<<" 0"<<std::endl;
        status(_("Pusher"));
      }
      else if(ch == 'd'){
        std::cout <<" "<<(unsigned)hix<<" 0 0 0"<<std::endl;
        status(_("Horisontal door"));
      }
    }
    else if(secport){
      std::cout << " " << (unsigned)hix << " " << (unsigned)hiy <<
        " 0 [0123] 0" << std::endl;
      secport = false;
      status("Teleport");
    }
    else{
      std::cout << ch <<" "<< (unsigned)hix <<" "<< (unsigned)hiy;
      if(ch == 's') std::cout << " [1-7]";
      if(ch == '[' or ch == ']' or ch == '_' or ch =='s')
        std::cout << _(" GROUP");
      else if(ch == 'b') std::cout << " assets/box_2.png";
      else if(ch == 'M' or ch == 'T' or ch == 'B'){
        switch(ch){
          case 'M': hm = true; break;
          case 'T': ht = true; break;
          case 'B': hb = true; break;
        }
        ch = '.'; shp = 0; co = &wall; status(_("Wall"));
      }
      if(ch == 'v'){lx = hix; co = &lift2; status(_("Set terminal position"));}
      else if(ch == 'h' or ch == 'p' or ch == 'd'){
        ly=hiy; co=&lift2; status(_("Set terminal position"));
      }
      else if(ch == 't'){secport=true; status(_("Place the second teleport"));}
      else std::cout << std::endl;
    }

    nodes[hix][hiy] = true;
    if(shp & 1) nodes[hix + 1][hiy] = true;
    if(shp & 2) nodes[hix][hiy + 1] = true;
    if(shp == 3) nodes[hix + 1][hiy + 1] = true;
  }
}

class Handler : public Funxor
{
  public:
    bool OnQuit(){done = true;}
    bool OnKeyDown(SDLKey key, SDLMod mod, Uint8 state){
      if(lx >= 0 or ly >= 0 or secport) return true;
      switch(key){
        case SDLK_b:
          ch = 'b';
          shp = 3;
          co = &brick;
          status(_("Brick"));
          break;
        case SDLK_g:
          ch = 'g';
          shp = 3;
          co = &glass;
          status(_("Glass brick"));
          break;
        case SDLK_y:
          ch = 'T';
          shp = 3;
          co = &target;
          status(ht?_("Target brick (W: One already placed!)"):_("Target brick"));
          break;
        case SDLK_COMMA:
          ch = 'B';
          shp = 1;
          co = &base;
          status(hb?_("Base (W: One already placed!)"):_("Base"));
          break;
        case SDLK_m:
          ch = 'M';
          shp = 3;
          co = &moovie;
          status(hm?_("Moovie (W: One already placed!)"):_("Moovie"));
          break;
        case SDLK_v:
          ch = 'v';
          shp = 1;
          co = &lift;
          status(_("Vertical lift"));
          break;
        case SDLK_h:
          ch = 'h';
          shp = 1;
          co = &lift;
          status(_("Horisontal platform"));
          break;
        case SDLK_t:
          ch = 't';
          shp = 3;
          co = &port;
          status(_("Teleport"));
          break;
        case SDLK_PERIOD:
          ch = '.';
          shp = 0;
          co = &wall;
          status(_("Wall"));
          break;
        case SDLK_LEFTBRACKET:
          ch = '[';
          shp = 2;
          co = &button;
          status(_("Button (pushed from the left)"));
          break;
        case SDLK_RIGHTBRACKET:
          ch = ']';
          shp = 2;
          co = &button;
          status(_("Button (pushed from the right)"));
          break;
        case SDLK_MINUS:
          ch = '_';
          shp = 1;
          co = &button;
          status(_("Button (pushed from above)"));
          break;
        case SDLK_p:
          ch = 'p';
          shp = 2;
          co = &lift;
          status(_("Pusher"));
          break;
        case SDLK_d:
          ch = 'd';
          shp = 0;
          co = &lift;
          status(_("Horisontal door"));
          break;
        case SDLK_s:
          ch = 's';
          shp = 0;
          co = &sensor;
          status(_("Sensor"));
        default:
          break;
      }
    }
    bool OnMouseMotion(Uint16 x, Uint16 y, Sint16 xrel, Uint16 yrel,
        Uint8 state){
      hix = lx > -1 ? lx : x / NS;
      hiy = ly > -1 ? ly : y / NS;
      if(ch == '.' and state & SDL_BUTTON(1))
        mark(hix, hiy);
    }
    bool OnMbuttonDown (Uint8 button, Uint16 x, Uint16 y, Uint8 state){
      if(button != 1) return 1;
      mark(hix, hiy);
    }
};

int main(int argc, const char *argv[]){

  //Locale stuff
  setlocale(LC_ALL, "");
  bindtextdomain("moovie", "../locale");
  textdomain("moovie");

  MAsdl::Init();
  Handler ha;
  Event ev;
  ev.Add(ha);

  disp = new Display(NX*NS, NY*NS + 24);
  disp->SetTitle(_("Moovie editor"));
  bg = new Surface(NX*NS, NY*NS);
  cur = new Surface(NX*NS, NY*NS);
  marks = new Surface(NX*NS, NY*NS);

  TTF_Init();
  font = TTF_OpenFont("xolonium.otf", 18);
  status(_("Wall"));

  bg->Convert();
  cur->Convert();
  marks->Convert();

  for(int y = 0; y < NY; y++){
    for(int x = 0; x < NX; x++){
      nodes[x][y] = 0;
    }
  }

  marks->FRect(Rect(0, 0, NX * NS, NY * NS), Color(128, 128, 128, 0));
  bg->FRect(Rect(0, 0, NX * NS, NY * NS), Color(128, 128, 128, 255));
  if(argc > 1){
    bg->SelfLoad(argv[1]);
  }

  std::cout << _("\n# START LEVEL #\n\n% NUM ") <<
    (argc > 1 ? argv[1]+3 : "assets/bg_NUM.png") << std::endl;

  // Edge walls:
  for(int i = 0; i < NX; i++){
    mark(i, 0, false);
    mark(i, 21, false);
  }
  for(int i = 1; i < NY; i++){
    mark(0, i, false);
    mark(31, i, false);
  }

  FPS fps(20);
  fps.Start();

  while(!done){
    cur->FRect(Rect(0, 0, NX * NS, NY * NS), Color(128, 128, 128, 0));
    cur->FRect(Rect(hix * NS, hiy * NS, shp&1 ? 2*NS : NS, shp&2 ? 2*NS : NS),
        Color(255, 20, 10, 200));
    disp->Blit(*bg);
    disp->Blit(*marks);
    disp->Blit(*cur);
    disp->Blit(*stat, noRect, Rect(4, NY * NS + 2, NX * NS, 20));
    disp->Update();

    fps.Delay();
    while(ev.Next());
  }

  std::cout << _("\n# END LEVEL #\n\n");

  TTF_Quit();
  MAsdl::Quit();
  return 0;
}
