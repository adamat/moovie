Moovie - level file format
==========================

Level file format is a plain text format. With some exceptions, it doesn't
matter what whitespace you use. Here are the items and their attributes:

# {text}
  A comment. Ignores everything until the end of line

% {id} {path/to/background}
  The id is a number and must be unique within file. The '%' must be the fist
  character on the line.
  
* {path/to/sprite} {frame dimensions} {frame count} {pixel coordinates} {opts}
  THIS WAS DELETED!
  An animation with no function. Frame dimensions are frame's width and
  height, frame count is the number of frames in every row and column,
  coordinates are in pixels of game scene.
  Options are a single number, that is a sum of options:
    HALF the timeout between loops (0 to 63 -> real time 0 to 126 frames)
    64 for Return option: the animation loops to the end and then back
    128 for Once option: the animation doesn't loop, only play once
    256 for Slow option: every frame is shown twice, resulting in slower play.

. {coordinates}
  A wall block

b {coordinates} {texture variant}
  A brick with the given sprite image

g {coordinates}
  A glass brick

T {coordinates}
  The target (winning) brick. There must be only one.

B {coordinates}
  The base, where the target brick is to be placed. There must be only one.

M {coordinates}
  Moovie - the player's character. There must be only one.

o {coordinates}
  A ball. Will be placed at the bottom center of the node specified.

The machines
------------

The machines are distinctive by possible belonging to group(s) (controlled by
buttons and sensors) and often their possibility to time themselves. The
timeouts are given in frames (Moovie runs at 30 FPS) and are reset whenever the
machine is triggered manually using a button.

[vh] {coordinates} {coordinate} {timeout} {texture variant} {groups}
  Vertical ('v') or horizontal ('h') lift. The third coordinate is the
  terminal position of the lift.

p {coordinates} {coordinate} {texture variant} {groups}
  Pusher - extensible arm that pushes bricks. The third coordinate marks the
  terminal position.

d {coordinates} {coordinate} {time to close} {closed [01]} {texture variant} {groups}
  Horizontal door. The third coordinate marks the terminal position. Time to
  close is time between the finish of closing and beginning of opening again
  (or vice versa if closed from the start). The starting position will always
  be solid (impenetrable), so you may want to define it inside a "wall".

| {coordinates} {time to close} {closed [01]} {texture variant} {groups}
  Vertical door. Always two nodes high. Time to close *only* works when the
  door is initialised closed.

t {coordinates} {coordinates} {timeout} {sprite setting [0123]} {texture variant} {groups}
  A pair of teleports. Sprite setting defines, which teleport will have its
  "body" rendered; 0 - none, 1 - first, 2 - second, 3 - both. The teleporting
  effect is rendered always.

[[]_] {coordinates} {texture variant} {group}
  Buttons - can be pushed from the left ('['), right (']') or above ('_'). It
  controls all machines in the given group.

s {coordinates} {type} {group}
  A sensor. Activates the given group, if an object of given type enters its
  coordinates. Type is a number representing flags, so a sum of numbers is like
  ORing the flags:
  | Nr | Type         |
  |----|--------------|
  | 1  | Bricks       |
  | 2  | Enemy        |
  | 4  | Moovie       |
  
  Use '6' to detect all creatures and '7' to detect everything (not machines,
  though).

Attributes
----------

There are several attributes that may need explanation:

{coordinates}
  Always a pair of x and y coordinates in game nodes, not pixels. If the
  object is larger than one node, these coordinates always denote the top-left
  corner.

{groups}
  A set of numbers separated by a whitespace. IMPORTANT: They MUST be
  terminated with a zero (hereby zero can't be a group number). Note that
  machines may be assigned multiple groups, whereas buttons may only control
  one.

{path/to/file}
  Paths are relative to the main directory.

{texture variant}
  Nuber (usually 0-2, but up to 29 for Bricks) denoting visual variant.


Order of items
--------------

'%' must be of course the first item in the level, there may be multiple in
the file, however, each starting a new level definition.

Items may "override" other items, if they are defined after them. For
example, you may place a button, even when on that position there is already
wall defined. Generally, it's a good idea to define the walls first.


TODO
----

Make this just a source code for levels; the final (loaded) file should be
binary, so either 1) Convert everything to numbers and then bytes, or
                  2) GZIP THIS CRAP!

